package com.sabris.oscript.parser;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Map;
import java.util.HashMap;

public class ConfigurationManager {
	Map<String, String> settings;
	
	public ConfigurationManager(String path) {
		try {
			File fXmlFile = new File(path);
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    Document doc = dBuilder.parse(fXmlFile);
		    
		    Element appSettings = (Element)doc.getElementsByTagName("appSettings").item(0);
		    NodeList addKeys = appSettings.getElementsByTagName("add");
		    this.settings = new HashMap<String, String>();
		    
		    for (int i = 0, sz=addKeys.getLength(); i<sz; i++) {
		    	Node nNode = addKeys.item(i);
		    	if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		    		Element eElement = (Element) nNode;
		    		
		    		this.settings.put( eElement.getAttribute("key"), eElement.getAttribute("value") );
		    	}
		    }
		} catch (Exception e) {
			e.printStackTrace();
	    }
	}
	
	public String appSettings(String key) {
	    if( settings.containsKey(key) ) {
	        return settings.get(key);
	    } else
	        return "";
	}
}
