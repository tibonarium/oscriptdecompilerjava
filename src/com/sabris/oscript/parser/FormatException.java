package com.sabris.oscript.parser;

public class FormatException extends Throwable {
    public FormatException(String message){
        super(message);
    }
}
