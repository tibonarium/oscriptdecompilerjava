package com.sabris.oscript.parser;

import java.util.List;
import java.util.ArrayList;

import java.io.IOException;

import com.sabris.oscript.parser.struct.ByteToken;
import com.sabris.oscript.parser.struct.ObjectID;
import com.sabris.oscript.parser.struct.OScriptFeature;
import com.sabris.oscript.parser.support.LazyStream;

public class LLObject {
    int szRegion;
    ObjectID id;
    int numOrphans;
    int numFeatures;
    
    ObjectID script_1_ID;
    ObjectID script_2_ID;
    
    List<ObjectID> orphans;
    List<OScriptFeature> features;
    
    public LLObject() {
        orphans = new ArrayList<ObjectID>();
        features = new ArrayList<OScriptFeature>();
    }
    
    public void readObjectHeader(LazyStream input) throws IOException, FormatException {
        ByteToken[] section;
        
        section = input.readU4();
        szRegion = ByteToken.convertSectionToInteger( section );    
        
        section = input.readU4(); // expecting 00 ce ab 89 
        
        section = input.readU2();
        numOrphans = ByteToken.convertSectionToInteger( section );
        
        section = input.readU2();
        numFeatures = ByteToken.convertSectionToInteger( section );
        
        section = input.readU2(); // sometimes this value equals numFeatures
        
        section = input.readU4();
        this.id = new ObjectID( section );
        
        section = input.readU4(); // expecting 01 00 00 00
        
        
    }
        
    public void readObjectOrphans(LazyStream input, int numOrphans ) throws IOException, FormatException {
        ByteToken[] section;
        ObjectID obj;
        
        for(int i=0; i<numOrphans; i++) {
            section = input.readU4();
            obj = new ObjectID(section);
            orphans.add( obj );
        }
    }

    public void readScriptID(LazyStream input) throws IOException, FormatException {
        ByteToken[] section;
        
        section = input.readU4();
        script_1_ID = new ObjectID(section);
            
        section = input.readU4();
        script_2_ID = new ObjectID(section);
    }

    public void readObjectFeatures(LazyStream input, int numFeatures ) throws IOException {
        ByteToken[] section, oddSection;
        OScriptFeature fr;
        int num;
        String str;

        for(int j=0; j<numFeatures; j++) {
            section = input.readU2();
            num = ByteToken.convertSectionToInteger( section );
            str = input.readString( num ); // get rid of NULL
            
            section = input.readU2(); // expecting 01 00
            
            oddSection = input.readU4();
            
            fr = new OScriptFeature(str, num, section, oddSection);
            this.features.add( fr );
        }

        input.skipNULL();
    }
    
    public void processInput(LazyStream input) {
        try {
            readObjectHeader(input);
            
            readObjectOrphans(input, this.numOrphans );
            
            readScriptID(input);
            
            readObjectFeatures(input, this.numFeatures );
        } catch(FormatException ex) {
            ex.printStackTrace();
        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
