package com.sabris.oscript.parser;

import com.sabris.oscript.parser.OScriptDecompiler.OType;
import com.sabris.oscript.parser.support.LazyByteTokenStream;
import com.sabris.oscript.parser.struct.*;
import com.sabris.oscript.parser.support.*;

import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.Arrays;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;

public class LLScript {
	final static String LF = System.lineSeparator();
    
	int numConst;
	int szByteCode;
	int szConstBlock;
	int numMapping;
	OScriptType retType;
	int numArgs;
	int numLocal;
	
	public ConstantPool constPool;
	public VariablePool varPool;
	public OScriptCode code;
	
	Map<String, ByteToken[][]> header;
	Map<String, ByteToken[][]> strangeSection;
	
	NavigableMap<Integer, Integer> mappingOpCode;

	String functionName;
	Long szSourceText;
	String filePath;
	String fileContent;
	public String decompiledSourceText;
	
	public LLScript() {
		constPool = new ConstantPool();
		varPool = new VariablePool();
		code = new OScriptCode();
		mappingOpCode = new TreeMap<Integer, Integer>();	
		decompiledSourceText = "";
	}
	
	
	
	public void readHeaderSection(LazyStream input) throws FormatException, IOException {
		// (1) Read first three(3) 16-byte lines
	    ByteToken[] section1, section2, section3, section4;
		
		header = new HashMap<String, ByteToken[][]>();

		section1 = input.readU2();
		section2 = input.readU2(); // expecting 00 00
		header.put("A", new ByteToken[][]{ section1, section2 });

		section1 = input.readU2(); 
		section2 = input.readU2(); // expecting 00 00
		header.put("B", new ByteToken[][]{ section1, section2 });

		section1 = input.readU2();
		section2 = input.readU2(); // expecting 18 00
		// check ( sectionB1 ).Equals( sectionC1 )
		header.put("C", new ByteToken[][]{ section1, section2 });

		section1 = input.readU2(); // expecting sectionD1[1]==e9
		section2 = input.readU2(); // expecting 50 55
		header.put("D", new ByteToken[][]{ section1, section2 });

		section1 = input.readU2(); // expecting 01 00
		section2 = input.readU2(); // number of constants
		header.put("E", new ByteToken[][]{ section1, section2 });
		
		numConst = ByteToken.convertSectionToInteger( header.get("E")[1] );

		section1 = input.readU2(); // Number of bytecodes			
		section2 = input.readU2(); // Num: variable types info size + constant pool size
		header.put("F", new ByteToken[][]{ section1, section2 });
		
		szByteCode = ByteToken.convertSectionToInteger( header.get("F")[0] );
		//szByteCode -= 2; // don't know why exactly
		szConstBlock = ByteToken.convertSectionToInteger( header.get("F")[1] );

		section1 = input.readU2(); // expecting 18 00
		section1 = input.readU2(); // expecting 40 00
		header.put("G", new ByteToken[][]{ section1, section2 });

		section1 = input.readU2();
		section2 = input.readU2();
		header.put("H", new ByteToken[][]{ section1, section2 });

		section1 = input.readU2(); // expecting 0f 00
		section2 = input.readU2(); // number of mapping code op to line number
		header.put("K", new ByteToken[][]{ section1, section2 });
		
		//numMapping = convertSectionToInteger( header["K"][1] );

		section1 = input.readU2(); // return type			
		section2 = input.readU2(); // Number of bytecodes			
		// check ( sectionL2 ).Equals( sectionF1 )
		header.put("L", new ByteToken[][]{ section1, section2 });
		
		retType = ByteToken.convertSectionToType( header.get("L")[0] );

		section1 = input.readU2(); // number of arguments			
		section2 = input.readU2(); // TODO: meaning of this number?
		section3 = input.readU2(); // number of local variables			
		section4 = input.readU2(); // TODO: do we need this section
		header.put("M", new ByteToken[][]{ section1, section2, section3, section4 });
		
		numArgs = ByteToken.convertSectionToInteger( header.get("M")[0] );
		numLocal = ByteToken.convertSectionToInteger( header.get("M")[2] );
	}
	
	public void skipAfterHeaderSection(LazyStream input) throws IOException {
        // Skip one 16-byte row
        input.readBytes( 16 );

		// Skip ReadU4(), ReadU4(), ReadU2()
		// So that reading constant can succeed
		// TODO: Or we can place reading of fake constant            
		input.readU4();
		input.readU4();
		//input.readU2();
	}
	
	public OScriptConstant readConstantDescription(LazyStream input) throws FormatException, IOException {
        Long offset;
        OScriptType type;
        ByteToken[] section1, section2, section3;
        
        // skip 6 bytes
        /* input.readU2(); // expecting "00 00"
        input.readU2(); // expecting "00 00"
        input.readU2(); // expecting "00 00"
        
        section1 = input.readU4();          
        section2 = input.readU4(); // expecting "00 00 00 00"
        section3 = input.readU2();
        
        type = convertSectionToType( section1 );
        offset = convertSectionToNumber( section3 ); */
        
        //>>>
        section1 = input.readU8();
        section2 = input.readU4();
        section3 = input.readU4(); // expecting "00 00 00 00"
        
        type = ByteToken.convertSectionToType( section2 );
        offset = ByteToken.convertSectionToLong( section1 );
        
        return new OScriptConstant(type, offset);
    }

	public void readConstantDescriptions(LazyStream input, int numConst) throws IOException {
		OScriptConstant oConstant;
		try {
    		for(int i=0; i<numConst; i++) {
    			oConstant = readConstantDescription( input );
                oConstant.index = i;
    			constPool.add(i, oConstant );
    		}
		} catch(FormatException ex) {
		    ex.printStackTrace();
		}
	}

	public void readByteCode(LazyStream input, int szBytecode) throws IOException {			
	    ByteToken[] bytes = input.readBytes( szBytecode );
		code.init( bytes );
	}

	public void readVariableTypes(LazyStream input, int numArgs, int numLocal) 
	        throws FormatException, IOException {
		OScriptType type;
		ByteToken[] section;
        input.skipNULL();
		
		for(int i=0; i<numArgs; i++) {
			section = input.readU4();
			type = ByteToken.convertSectionToType( section );
			this.varPool.addArgument(i, type);
		}
		
		for(int j=0; j<numLocal; j++) {
			section = input.readU4();
			type = ByteToken.convertSectionToType( section );
			this.varPool.addLocal(j, type);
		}

		/*for(int i=0, size=numArgs+numLocal; i<size; i++) {
			section = input.readU4();
			type = convertSectionToType( section );
			this.varPool.add(i, type);
		} */
	}

	public void readConstantValues(LazyStream input) throws IOException {
        OScriptConstant oConst;
		String val1, val2;
		
		int sz = constPool.getKeys().size();
		for(int k=0; k<sz; k++) {
			oConst = constPool.getConstant( k );
			if( oConst.otype==OType.String ) {
				val1 = input.readString();
				oConst.value = val1;
			} else if( oConst.otype==OType.XLate ) {
				val1 = input.readString();
				val2 = input.readString();
				oConst.value = String.format("%s.%s", val1, val2);
			}
		}
	}

	public void readStrangeSection(LazyStream input) throws IOException {
		// u2 u2=00 00 u2[mappingOpCode_size] u2=0a 00
		// u2 u4=01 00 18 00 u4 u4=00 00 00 00(νε βρεγδΰ) u4 u4=00 00 00 00
		
		strangeSection = new HashMap<String, ByteToken[][]>();
		ByteToken[] section1, section2, section3, section4;

		section1 = input.readU2();
		section2 = input.readU2(); // expecting "00 00"
		section3 = input.readU2(); 
		// expecting ( sectinA1 ).rquals( sectionA3 )
		section4 = input.readU2(); // expecting "0a 00"
		strangeSection.put("A", new ByteToken[][]{ section1, section2, section3, section4 });
		
		section1 = input.readU2(); // whodonuit
		section2 = input.readU4(); // expecting "01 00 18 00"
		strangeSection.put("B", new ByteToken[][]{ section1, section2 });
		
		numMapping = ByteToken.convertSectionToInteger(section1);
		
		section1 = input.readU4();
		section2 = input.readU4(); // expecting "00 00 00 00", but not always
		section3 = input.readU4();
		section4 = input.readU4(); // expecting "00 00 00 00"
		strangeSection.put("C", new ByteToken[][]{ section1, section2, section3, section4 });
	}

	public void readVariableMapping(LazyStream input, int nArgs, int nLocal) throws IOException {
		//int size = nArgs + nLocal;
		Long offset;
		ByteToken[] section;

		/*for(int i=0; i<size; i++) {
            section = input.readU2();
			offset = convertSectionToNumber( section );
			this.varPool.setOffsetWithKey(i, offset); // i is the key	
		}*/
		
		for(int i=0; i<nArgs; i++) {
            section = input.readU2();
            offset = ByteToken.convertSectionToLong( section );
            this.varPool.setArgumentOffsetWithKey(i, offset);
        }
        
        for(int j=0; j<nLocal; j++) {
            section = input.readU2();
            offset = ByteToken.convertSectionToLong( section );
            this.varPool.setLocalOffsetWithKey(j, offset);
        }
	}

	public void readVariableStrings(LazyStream input, int nArgs, int nLocal) {
		try {
    	    OScriptVariable oVar;
    	    input.skipNULL();
    	    
    		// first one is the function name
    		String val = input.readString();
    		this.functionName = val;
    		
    		/*int sz = varPool.getKeys().size();
    		for(int k=0; k<sz; k++) {
    			oVar = varPool.getVariable( k );
    			val = input.readString();
    			oVar.name = val;
    		}*/
    		
    		for(int i=0; i<nArgs; i++) {
    		    oVar = varPool.getArgument( i );
    		    val = input.readString();
    		    oVar.name = val;
            }
            
            for(int j=0; j<nLocal; j++) {
                oVar = varPool.getLocal( j );
                val = input.readString();
                oVar.name = val;
            }
		} catch(FormatException ex) {
		    ex.printStackTrace();
		} catch(IOException iex) {
		    iex.printStackTrace();
		}
		
	}

	// TODO: nMapping is currently depricated
	public void readOpCodeMapping(LazyStream input, int nMapping) throws IOException {
        ByteToken[] section1=null, section2=null; 
        ByteToken[] section3, section4;
		int idxLine, idxOpCode;
        input.skipNULL();

        // TODO: how to make opcode mapping reading more precise?
        for(int j=0; j<nMapping; j++) {
            section1 = input.readU2();
            section2 = input.readU2(); // usually section1.equals(section2) but not always
            
            section3 = input.readU2(); // expecting "00 00"
            section4 = input.readU2();

            idxOpCode = ByteToken.convertSectionToInteger(section4);
            idxLine = ByteToken.convertSectionToInteger(section1);

            this.mappingOpCode.put(idxLine, idxOpCode);
        }
		
        /*while( !input.endOfStream() ) {				
			section1 = input.readU2();
			section2 = input.readU2();
			// check ( section1 ).Equals( section2 )
            if ( section1[0].equals( section2[0] ) && section1[0].equals( section2[0] ) ) {

                section3 = input.readU2(); // expecting "00 00"
                section4 = input.readU2();

                idxOpCode = convertSectionToInteger(section4);
                idxLine = convertSectionToInteger(section1);

                this.mappingOpCode.put(idxLine, idxOpCode);
            }
            else break;
		} */
		
		// info about text			
		/*if( section1!=null && section2!=null ) {
			String[] sec = new String[]{ section1[0], section1[1], section2[0], section2[1] };
			szSourceText = convertSectionToNumber(sec);
		}*/
	}

	public void readTextInfo(LazyStream input) throws IOException {
		input.readU4(); // unknown section
		ByteToken[] section = input.readU4();
		szSourceText = ByteToken.convertSectionToLong(section);
	}
	
	public int findSOH(ByteToken[] src, int offset ) {
		int sz = src.length;
        int index = offset;
		while( index<sz ) {
			if( src[ index ].value == (char)0x01 ) break;
			index++;
		}
		return index;
	}
	
	public String readString(ByteToken[] src, int start, int end) throws FormatException {
		if( start>end )
            throw new FormatException("start index can't be bigger than end index");
		if( end>=src.length )
            throw new FormatException("end index can't be bigger or equal to input array length");
		
		char sym;
		StringBuilder sb = new StringBuilder();

        for (int i = start; i <= end; i++) {
			if( src[i].value == (char)0x01 ) break;
			sym = (char) src[ i ].toInt32();
			sb.append( sym );
		}
		return sb.toString();
	}

	public void readSourceText(LazyStream input, Long _szSourceText ) throws IOException {
		if( _szSourceText<=0 ) return;
			
		try {
            ByteToken[] src = input.readByteTokens( _szSourceText );
    		int index = findSOH( src, 1 ); // SOH will be also located before file path
    		
    		this.filePath = readString(src, 1, index-1);
    		this.fileContent = readString(src, index+1, src.length-1);
		} catch(FormatException ex) {
		    ex.printStackTrace();
		}
	}
			
	public void processInput(LazyStream input) {
		try {
    		readHeaderSection(input);
    		skipAfterHeaderSection(input);
    		
    		readConstantDescriptions(input, numConst );
    		readByteCode(input, szByteCode );
    		readVariableTypes(input, numArgs, numLocal );
    		readConstantValues(input);
    		
    		readStrangeSection(input);
    		
    		readVariableMapping(input, numArgs, numLocal );
    		readVariableStrings(input, numArgs, numLocal );
    		readOpCodeMapping(input, numMapping );
    		
    		readTextInfo(input);
    		readSourceText(input, szSourceText );
    	} catch(FormatException ex) {
    	    ex.printStackTrace();
    	} catch(IOException iex) {
    	    iex.printStackTrace();
    	}
	}
	
	public static String getTabs(int indent) {
	    if( indent>0 )
	        return String.join("", Collections.nCopies(indent, "\t"));
	    else
	        return "";
    }
    
    public static void dumpLine(OutputStreamWriter sw, int indent, String line) throws IOException {
        sw.write(getTabs(indent) + line, 0, indent+line.length());
        sw.write(LF, 0, LF.length());
    }
	
	public static void dumpHeader(OutputStreamWriter sw, int indent, String line) throws IOException {
		sw.write(LF, 0, LF.length());
		dumpLine(sw, 0, line);
		sw.write(LF, 0, LF.length());
	}
	
	public static void dumpHeaderDecorated(OutputStreamWriter sw, int indent, String line) throws IOException {
		sw.write(LF, 0, LF.length());
		sw.write("//==========", 0, 12);
		sw.write(LF, 0, LF.length());
		dumpLine(sw, 0, line);
		sw.write("//==========", 0, 12);
		sw.write(LF, 0, LF.length());
		sw.write(LF, 0, LF.length());
	}
	
	public void dump4USections(OutputStreamWriter sw) throws IOException {			
		StringBuilder sb = new StringBuilder();
        int i;
					
		sb.setLength(0);
		dumpLine(sw, 0, "// section A");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("A")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // xx xx 00 00");
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section B");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("B")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // xx xx 00 00");
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section C");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("C")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // xx xx 18 00");
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section D");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("D")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // xx e9 50 55");
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section E");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("E")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // 01 00 xx xx (numConst)");
		dumpLine(sw, 0, sb.toString());
		
		dumpHeader(sw, 0, String.format("numConst: %d", numConst) );

		sb.setLength(0);
		dumpLine(sw, 0, "// section F");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("F")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // xx xx (szByteCode+2) xx xx (szConstBlock)");
		dumpLine(sw, 0, sb.toString());
		
		sw.write(LF, 0, LF.length());
		dumpLine(sw, 0, String.format("szByteCode: %d", szByteCode) );
		dumpLine(sw, 0, String.format("szConstBlock: %d", szConstBlock) );
		sw.write(LF, 0, LF.length());

		sb.setLength(0);
		dumpLine(sw, 0, "// section G");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("G")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // 18 00 40 00");
		dumpLine(sw, 0, sb.toString());

		sb.setLength(0);
		dumpLine(sw, 0, "// section H");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("H")[i].toString() ) );
			sb.append(" ");
		}
		dumpLine(sw, 0, sb.toString());

		sb.setLength(0);
		dumpLine(sw, 0, "// section K");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("K")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // 0f 00 xx xx");
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section L");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("L")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // xx xx (retType) xx xx (szByteCode+2)");
		dumpLine(sw, 0, sb.toString());
		
		dumpHeader(sw, 0, String.format("retType: %s", retType.toString()) );

		sb.setLength(0);
		dumpLine(sw, 0, "// section M");
		for(i=0; i<4; i++) {
			sb.append( String.join( " ", header.get("M")[i].toString() ) );
			sb.append(" ");
		}
		sb.append(" // xx xx (numArgs) xx xx xx xx (numLocal)");
		dumpLine(sw, 0, sb.toString());
					
		sw.write(LF, 0, LF.length());
		dumpLine(sw, 0, String.format("numArgs: %d", numArgs) );
		dumpLine(sw, 0, String.format("numLocal: %d", numLocal) );
		sw.write(LF, 0, LF.length());
	}
	
	public void dump16USections(OutputStreamWriter sw) throws IOException {
		StringBuilder sb = new StringBuilder();
        int i;						
		
		dumpLine(sw, 0, "// Row 01: [section A] [section B] [section C] [section D]");
		dumpLine(sw, 0, "// Row 02: [section E] [section F] [section G] [section H]");
		dumpLine(sw, 0, "// Row 03: [section K] [section L] [section M]x4");
		sw.write(LF, 0, LF.length());
		
		sb.setLength(0);
		sb.append("[");
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("A")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("B")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("C")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", header.get("D")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");			
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		sb.append("[");			
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("E")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("F")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("G")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("H")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		dumpLine(sw, 0, sb.toString());

		sb.setLength(0);
		sb.append("[");			
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("K")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<2; i++) {				
			sb.append( String.join( " ", header.get("L")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");
		sb.append("[");
		for(i=0; i<4; i++) {				
			sb.append( String.join( " ", header.get("M")[i].toString() ) );
			sb.append(" ");
		}
		sb.append("] ");			
		dumpLine(sw, 0, sb.toString());
		
		sw.write(LF, 0, LF.length());
		sb.setLength(0);
		sb.append("// ");
		sb.append("[xx xx 00 00] ");
		sb.append("[zz zz 00 00] ");
		sb.append("[zz zz 18 00] ");
		sb.append("[xx e9 50 55] ");
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		sb.append("// ");
		sb.append("[01 00 xx xx (numConst)] ");
		sb.append("[xx xx (szByteCode+2) xx xx (szConstBlock)] ");
		sb.append("[18 00 40 00] ");
		sb.append("[?? ?? ?? ??] ");
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		sb.append("// ");
		sb.append("[0f 00 xx xx] ");
		sb.append("[xx xx (retType) xx xx (szByteCode+2)] ");
		sb.append("[xx xx (numArgs) xx xx xx xx (numLocal)] ");
		dumpLine(sw, 0, sb.toString());
					
		sw.write(LF, 0, LF.length());
		dumpLine(sw, 0, String.format("numConst: %d", numConst) );
		dumpLine(sw, 0, String.format("szByteCode: %d", szByteCode) );
		dumpLine(sw, 0, String.format("szConstBlock: %d", szConstBlock) );
		dumpLine(sw, 0, String.format("retType: %s", retType.toString()) );
		dumpLine(sw, 0, String.format("numArgs: %d", numArgs) );
		dumpLine(sw, 0, String.format("numLocal: %d", numLocal) );
		sw.write(LF, 0, LF.length());
	}
	
	public void dumpHeaderSection(OutputStreamWriter sw) throws IOException {
		dumpHeaderDecorated(sw, 0, "// Header section");
		
		//dump4USections(sw);
		dump16USections(sw);
	}
		
	public void dumpConstants(OutputStreamWriter sw) throws IOException {
		dumpHeaderDecorated(sw, 0, "// Constant pool");
		OScriptConstant oConst;
		for(Map.Entry<Integer, OScriptConstant> pair : constPool.constants.entrySet()) {
		    oConst = pair.getValue();
		    dumpLine(sw, 0, String.format("%s\t\t#%02d\t%s"
		            , oConst.otype.toString()
		            , pair.getKey()
		            , oConst.toString() ) );
		}
	}
	
	public void dumpVariables(OutputStreamWriter sw) throws IOException {
		dumpHeaderDecorated(sw, 0, "// Variable pool");
		
		dumpLine(sw, 0, "// Arguments");
		for(Map.Entry<Integer,OScriptVariable> pair : varPool.arguments.entrySet()) {
			dumpLine(sw, 0, String.format("%s\t\t#%02d\t%s", pair.getValue().varType.toString(), pair.getKey(), pair.getValue().name) );
		}
		
		sw.write(LF, 0, LF.length());
        dumpLine(sw, 0, "// Locals");
		for(Map.Entry<Integer,OScriptVariable> pair : varPool.locals.entrySet()) {
			dumpLine(sw, 0, String.format("%s\t\t#%02d\t%s", pair.getValue().varType.toString(),pair.getKey(), pair.getValue().name) );
		}
	}
	
	public void dumpByteCode(OutputStreamWriter sw) throws IOException {
		dumpHeaderDecorated(sw, 0, "// ByteCode Full");
		String byteCodeContent = code.toString();
		
		sw.write(byteCodeContent, 0, byteCodeContent.length() );
		
		sw.write(LF, 0, LF.length());
		dumpHeaderDecorated(sw, 0, "// ByteCode");
		int cur = 0;
		int start, end;
		int lineIndex;
		Map.Entry<Integer, Integer> next;
				
		for( Map.Entry<Integer, Integer> e : mappingOpCode.entrySet() ) {
		    lineIndex = e.getKey();
		    start = e.getValue();
		    
		    next = mappingOpCode.higherEntry(e.getKey());
		    if( next!=null ) {
		        end = next.getValue();
		    } else {
		        end = szByteCode-1;
		    }
		    
		    if( cur<start ) {
                dumpLine(sw, 0, code.toString(cur, start-1) );
                cur = start;
            }
            dumpLine(sw, 0, String.format("// line %03d", lineIndex) );
            if( start<=end ) { 
                dumpLine(sw, 0, code.toString(start, end-1) );
                cur = end;
            }
		}
		
		if( cur<szByteCode ) {
			dumpLine(sw, 0, code.toString(cur, szByteCode-1) );
			cur = szByteCode;
		}
		
		sw.write(LF, 0, LF.length());
	}
		
	public void DumpStrangeSection(OutputStreamWriter sw) throws IOException {
		dumpHeaderDecorated(sw, 0, "// Strange section");
		StringBuilder sb = new StringBuilder();
        int i;
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section A");
		for(i=0; i<4; i++) {
			sb.append( String.join( " ", strangeSection.get("A")[i].toString() ) );
			sb.append(" ");
		}
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section B");
		for(i=0; i<2; i++) {
			sb.append( String.join( " ", strangeSection.get("B")[i].toString() ) );
			sb.append(" ");
		}
		dumpLine(sw, 0, sb.toString());
		
		sb.setLength(0);
		dumpLine(sw, 0, "// section C");
		for(i=0; i<4; i++) {
			sb.append( String.join( " ", strangeSection.get("C")[i].toString() ) );
			sb.append(" ");
		}
		dumpLine(sw, 0, sb.toString());
		
	}
	
	public void dumpOpCodeMapping(OutputStreamWriter sw) throws IOException {
		dumpHeaderDecorated(sw, 0, "// OpCode mapping");
		
		for(Map.Entry<Integer, Integer> pair : mappingOpCode.entrySet()) {
			dumpLine(sw, 0, String.format("Line: %d\t\tOpCode: %d", pair.getKey(), pair.getValue()) );
		}
	}
	
	public void dumpDecompiledSourceText(OutputStreamWriter sw) throws IOException {
        if (decompiledSourceText==null || decompiledSourceText.isEmpty()) return;
		
		int idx = 0;
		dumpHeaderDecorated(sw, 0, "// Decompiled source text");
		
		String[] lines = this.decompiledSourceText.split("\r\n");
		
		lines = Arrays.stream(lines)
	         .filter(s -> (s != null && s.length() > 0))
	         .toArray(String[]::new);
		
		for(int i=0, sz=lines.length; i<sz; i++) {
			dumpLine(sw, 0, String.format("%03d %s", idx++, lines[i]) );
		}
	}
		
	public void dumpSourceText(OutputStreamWriter sw) throws IOException {
		int idx = 0;
		dumpHeaderDecorated(sw, 0, "// Source text");			
		dumpLine(sw, 0, String.format("%03d %s", idx++, this.filePath) );
		
		String[] lines = this.fileContent.split("\r|\n");
			
		for(int i=0, sz=lines.length; i<sz; i++) {
			dumpLine(sw, 0, String.format("%03d %s", idx++, lines[i]) );
		}
	}
	
	public void dumpLLScript(String targetDir, String targetName) {
	    try {
	        OutputStream fStream = new FileOutputStream(targetDir + "\\" + targetName);	        
	        OutputStreamWriter sw = new OutputStreamWriter(fStream, "UTF-8");
    
            //int indent = 0; TODO: future implementation
            
    		dumpHeaderSection(sw);
    		
    		dumpConstants( sw );
    		dumpVariables( sw );
    		dumpByteCode( sw );
    		
    		//dumpStrangeSection( sw );			
    		dumpOpCodeMapping( sw );
    		
    		dumpDecompiledSourceText( sw );
    		dumpSourceText( sw );
    		
    		sw.close();
    	} catch(IOException ex) {
    	    ex.printStackTrace();
    	}
	}
}
