package com.sabris.oscript.parser;

import static com.sabris.oscript.parser.struct.ByTk.*;

import com.sabris.oscript.parser.struct.*;
import com.sabris.oscript.parser.support.*;
import com.sabris.oscript.parser.expr.*;
import com.sabris.oscript.parser.expr.math.*;
import com.sabris.oscript.parser.expr.logic.*;
import com.sabris.oscript.parser.expr.special.*;
import com.sabris.oscript.parser.expr.func.*;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.Stack;

public class OScriptDecompiler {
	private static final boolean DEV_READ_OLL = false;
	private static final ByteToken endToken = new ByteToken("03");
	
	private static final boolean DEV_DEBUG = true;
	
	private List<ByteToken> tokens;
    private int tokenLength;
    private int tokenIndex;
    public ConstantPool constPool;
    public VariablePool varPool;
    
    protected List<Expression> output;
    protected int outIdx;
    //int indent;
    protected StringBuilder lineBuf;
    protected boolean unknownBytecode;
    
    State state;
	
	public enum OType {
        UnknownType(0) {
            @Override
            public String toString() { return "UnknownType"; }
        },
        Boolean(1) {
            @Override
            public String toString() { return "Boolean"; }
        },
        Date(2) {
            @Override
            public String toString() { return "Date"; }
        },
        Error(3) {
            @Override
            public String toString() { return "Error"; }
        },
        Integer(4) {
            @Override
            public String toString() { return "Integer"; }
        },
        List(5) {
            @Override
            public String toString() { return "List"; }
        },
        Set(6) {
            @Override
            public String toString() { return "Set"; }
        },
        Real(7) {
            @Override
            public String toString() { return "Real"; }
        },
        String(8) {
            @Override
            public String toString() { return "String"; }
        },
        Assoc(9) {
            @Override
            public String toString() { return "Assoc"; }
        },
        Object(10) {
            @Override
            public String toString() { return "Object"; }
        },
        RecArray(11) {
            @Override
            public String toString() { return "RecArray"; }
        },
        Record(12) {
            @Override
            public String toString() { return "Record"; }
        },
        ObjRef(13) {
            @Override
            public String toString() { return "ObjRef"; }
        },
        Dynamic(14) {
            @Override
            public String toString() { return "Dynamic"; }
        },
        Frame(15) {
            @Override
            public String toString() { return "Frame"; }
        },
        File(16) {
            @Override
            public String toString() { return "File"; }
        },
        Script(17) {
            @Override
            public String toString() { return "Script"; }
        }, 
        Point(18) {
            @Override
            public String toString() { return "Point"; }
        },
		XLate(19) {
            @Override
            public String toString() { return "XLate"; }
        };
		
		private int value;
        
        private OType(int value) { this.value = value; }
        
        public int toValue() { return value; }
    }

    public enum RefType {
        ValueType(0),
        ReferenceType(1);
        
        private int value;
        
        private RefType(int value) { this.value = value; }
    }
    
    public enum State {
        Normal,
        InsideDefault;
    }
		
	public OScriptDecompiler(OScriptDecompiler prev, /*int indent,*/ List<ByteToken> _tokens) {
		constPool = prev.constPool;
		varPool = prev.varPool;
		tokens = _tokens;
		tokenLength = tokens.size();
		tokenIndex = 0;
		
		output = new ArrayList<Expression>();
		outIdx = 0;
		//this.indent = indent;
		lineBuf = new StringBuilder();
		unknownBytecode = false;
		
		state = State.Normal;
	}
		
	public OScriptDecompiler(LLScript src) {
		constPool = src.constPool;
		varPool = src.varPool;
        tokens = src.code.getByteTokens();
		tokenLength = tokens.size();
		tokenIndex = 0;

        output = new ArrayList<Expression>();
		outIdx = 0;
		//indent = 0;
		lineBuf = new StringBuilder();
		unknownBytecode = false;
		
		state = State.Normal;
	}		
	
	public void setTokens(List<String> input) {
		for(String str : input) {
			tokens.add(new ByteToken(str));
		}
		tokenLength = tokens.size();
	}
	
	public void initConstantPool(String[] constants) {
		constPool = new ConstantPool(constants);
	}
	
	public boolean isNextToken() {
		return tokenIndex < tokenLength;
	}
	
	public int getTokenIndex() {
	    return tokenIndex;
	}
	
	public ByteToken nextToken() {
		if( tokenIndex>=tokenLength ) return new ByteToken();
		return tokens.get( tokenIndex++ );
	}
	
	public ByteToken peekNextToken() {
	    if( tokenIndex>=tokenLength ) return new ByteToken();
	    return tokens.get( tokenIndex );
	}
	
	public List<ByteToken> nextTokens( int num ) {
		if( tokenIndex>=tokenLength ) return new ArrayList<ByteToken>();
		List<ByteToken> sub;
		int sz = Math.min(num, tokenLength-tokenIndex);
		sub = tokens.subList(tokenIndex, tokenIndex+sz);
		tokenIndex += sz;
		return sub;
	}
	
	public List<ByteToken> peekNextTokens(int start, int end) {
	    if( start>end ) return new ArrayList<ByteToken>();
	    if( start>tokens.size() ) return new ArrayList<ByteToken>();
	    
	    List<ByteToken> sub;
	    int iEnd = Math.min(end, tokenLength);
        sub = tokens.subList(start, iEnd);
	    return sub;
	}
	
	public void moveTokenIndex( int end ) {
	    tokenIndex = Math.min(end, tokenLength-1);
	}
	
	public void restorePrevTokens( int num ) {
	    if( tokenIndex-num>0 ) tokenIndex -= num;
	}
	
	public int byteToInt( ByteToken token ) throws FormatException {
	    if( token.value==(char)0 ) throw new FormatException("ByteToken is empty");
		return (int)token.value;
	}
	
	public int byteToInt( String val ) throws FormatException {
        if( val.length()==0 ) throw new FormatException("ByteToken is empty");
        return Integer.parseInt(val, 16);
    }
	
	public int u2ToInt( ByteToken token1, ByteToken token2 ) throws FormatException {
	    int b1 = byteToInt( token1 );
	    int b2 = byteToInt( token2 );
	    int num = (b1<<8) + b2;
	    
	    if( num<tokenLength ) {
	        return num;
	    } else {
	        String val1 = token1.hex;
	        String val2 = token2.hex;
	        Long ln = Long.parseUnsignedLong("ffffffffffff" + val1 + val2, 16);
	        return ln.intValue();
	    }
	}
	
	public void addExpression(Expression expr) {
	    //if( this.indent<0 ) this.indent=0;
	    output.add(outIdx++, expr);
	}
	
	public Expression removeExpression() {
	    if( output.size()>1 ) {
	        outIdx--;
	        return output.remove( output.size()-1 );	        
	    }
	    return new BasicExpression();
	}
	
	public void addText(String text) {
		lineBuf.append( text );
	}
	
	public void check() {
		if( unknownBytecode ) {
            if (lineBuf.length() > 0) {
				Expression expr = new StringExpression(lineBuf.toString());
				//expr.indent = indent;
				output.add(outIdx++, expr);					
			}
			unknownBytecode = false;
		}
	}
	
	public static void writeLine(StringBuilder sb, int indent, String expr) {
	    String padding;
	    if( indent>0 )
            padding = String.join("", Collections.nCopies(indent, "\t"));
        else
            padding = "";
	    sb.append( padding ).append( expr );
        sb.append( System.lineSeparator() );
	}
	
	public static String getSourceText(List<Expression> output) {
	    StringBuilder sb = new StringBuilder();
        int sz = output.size();
        int indent = 0;
        
        Expression expr;
        for(int k=0; k<sz; k++) {
            expr = output.get(k);
            indent = expr.toString(sb, indent);
            
            //>>>
            /* indent = output.get( k ).indent;
            if( indent>0 )
                padding = String.join("", Collections.nCopies(indent, "\t"));
            else
                padding = "";
            
            sb.append( padding ).append( output.get( k ).toString() );
            sb.append( System.lineSeparator() ); */
        }
        return sb.toString();
	}
	
	public void removeLastJump(List<ByteToken> subroutine, List<Expression> expressions) {
        int sz = subroutine.size();
        if( sz>3 ) {
            ByteToken token = subroutine.get( sz-3 );
            if( (int)token.value>0 && token.value==(char)0x03 ) {
                if( expressions.size()>1 ) {
                    expressions.remove( expressions.size()-1 );
                }
            }
        }
	}
	
	public Expression concatExpressions(List<Expression> expressions) {
	    if( expressions.size()==0 ) return new BasicExpression();
	    
	    Expression expr;
	    int sz = expressions.size();
        if( sz > 1) {
            StringBuilder sb = new StringBuilder();
            for(int k=0; k<sz; k++) {
                sb.append( expressions.get( k ) ).append(" ");
            }                       
            expr = new StringExpression(sb.toString());
        } else {
            expr = expressions.get( 0 );
        }
        return expr;
	}
	
	public void processForExpression(DataPoint data) throws FormatException {
	    boolean processingFor = true;
	    
	    // previous tokens were "e6 eb"
	    // expecting "84 a8" and then "ad"
	    ByteToken token;
	    token = nextToken();
	    token = nextToken();
	    
	    Expression LH, RH, expr;
	    int index, num, b1, b2;
	    OScriptVariable locVar;
	    
	    List<ByteToken> subroutine;
        OScriptDecompiler proc;
        List<Expression> expressions;
        int sz;
	    
	    while( processingFor && isNextToken() ) {
	        token = nextToken();
            
            // TODO: move to switch()
            switch( token.value ) { 
            case xAD: //"ad":
                check();
                RH = data.remove();
                
                token = nextToken();
                index = byteToInt(token);
                
                locVar = varPool.getLocal( index );
                LH = new VariableExpression( locVar );
                
                expr = new ForeachExpression(LH, RH);                
                addExpression( expr );
                //indent++;
                
                break;
            case x03: // "03":
                token = nextToken();
                b1 = byteToInt( token );
                token = nextToken();
                b2 = byteToInt( token );
                num = ( b1<<8 ) + b2;
                       
                // (2) process code
                num -= 3; // shift to correct offset
                subroutine = nextTokens( num );
            
                proc = new OScriptDecompiler(this, /*indent,*/ subroutine );
                expressions = proc.codeToExpression();
                sz = expressions.size();
                
                for(int k=0; k<sz; k++) {
                    addExpression( expressions.get( k ) );
                }
                
                /* // (3) check three last tokens for else/end commands
                sz = subroutine.size();
                if( sz>3 ) {
                    token = subroutine.get( sz-3 );
                    if( token.value.length()>0 && token.value.equals("03") ) {
                        removeExpression();
                        restorePrevTokens( 3 );
                        break;
                    }
                }*/
                
                break;
            case x20: // "20":
                // TODO: here (4 bytes) some condition that return boolean is stored
                // How exactly it is stored?
                //20 (already done) e7 02 31
                token = nextToken();
                token = nextToken();
                token = nextToken();
                
                //09 00 0c
                token = nextToken();
                token = nextToken();
                token = nextToken();
                
                //e6 e1 03 ef
                token = nextToken();
                token = nextToken();
                token = nextToken();
                token = nextToken();
                
                //ad 00
                token = nextToken();
                token = nextToken();
                
                //03 ff c9 e4 03 
                token = nextToken();
                token = nextToken();
                token = nextToken();
                token = nextToken();
                token = nextToken();
                
                //indent--;
                addExpression( new EndExpression() );
                processingFor = false;
                break;
            default:
                break;
            }
	    }
	}

	public List<Expression> codeToExpression() {
	    DataPoint data = new DataPoint();
	    return codeToExpression(data);
	}
	
	public List<Expression> codeToExpression(DataPoint data) {
		ByteToken token, swToken;
		int index, num;
		int b1, b2;
		String hexCode;
		
		//String str;
		//StringExpression str;
		
		Map<Integer, GotoExpression> gotoMap = new HashMap<Integer, GotoExpression>();
		
        Expression expr, methodExpr;
        //Expression codeExpr;
        Expression LH, RH;

        MethodArgument args;
		
		OScriptVariable locVar, argVar;
		
		List<ByteToken> subroutine;
		OScriptDecompiler proc;
		List<Expression> expressions;
		int sz;
		StringBuilder sb;
		
		// switch variables
		int curIndex, defaultIndex;
		int defaultJump;
        NavigableMap<Integer, Integer> caseJump;
        int constID, jump;
        
        OScriptConstant oConst;
        Map.Entry<Integer, Integer> next;
        int start, end;
        
        Stack<Expression> reverseStack;
        
        GotoExpression gotoExpr;
		
		try {
		
		while( isNextToken() ) {
			token = nextToken();
			
			// TODO: move to switch()
			switch( token.value ) {				
				case xA5: // "a5": // loadconst
					check();
					token = nextToken();
					index = byteToInt( token );
					oConst = constPool.getConstant( index );
					
					if( oConst.otype==OType.String )
					    data.add( new StringExpression(oConst) );
					else
					    data.add( new BasicExpression(oConst) );
					break;
				case xAB: // "ab": // loadlocal
					check();
					token = nextToken();
					index = byteToInt( token );
					locVar = varPool.getLocal( index );
				
					data.add( new VariableExpression( locVar ) );
					break;
				case xAC: // "ac": // storelocal
					check();
					token = nextToken();
					index = byteToInt( token );
					token = nextToken(); //  expecting e3
					
					RH = data.remove();
					locVar = varPool.getLocal( index );
					LH = new VariableExpression( locVar );
                    expr = new AssignmentExpression(LH, RH);

                    addExpression( expr );
					break;
				case xAD: // "ad": // storelocal
                    check();
                    token = nextToken();
                    index = byteToInt( token );
                    
                    RH = data.remove();
                    locVar = varPool.getLocal( index );
                    LH = new VariableExpression( locVar );
                    expr = new AssignmentExpression(LH, RH);

                    addExpression( expr );
                    break;
				case xAE: // "ae": // loadarg
					check();
					token = nextToken();
					index = byteToInt( token );
					argVar = varPool.getArgument( index );
				
					data.add( new VariableExpression( argVar ) );
					break;
				case xAF: // "af": // storearg
					check();
					token = nextToken();
					index = byteToInt( token );
					token = nextToken(); //  expecting e3
					
					RH = data.remove();
					argVar = varPool.getArgument( index );
                    LH = new VariableExpression( argVar );
                    expr = new AssignmentExpression(LH, RH);

                    addExpression( expr );						
					break;
				case xC7: // "c7": // getglobal
					check();
					expr = data.remove();
					expr = new GlobalVarExpression(expr);
					data.add( expr );
					break;
				case xA8: // "a8":
					check();
					data.add( new StringExpression("Undefined") );
					break;
					
				case x72: // "72":
                    check();
                    data.add( new IntegerExpression(-18) );
                    break;
				case x73: // "73":
                    check();
                    data.add( new IntegerExpression(-17) );
                    break;
				case x74: // "74":
                    check();
                    data.add( new IntegerExpression(-16) );
                    break;
				case x75: // "75":
                    check();
                    data.add( new IntegerExpression(-15) );
                    break;
				case x76: // "76":
                    check();
                    data.add( new IntegerExpression(-14) );
                    break;
				case x77: // "77":
                    check();
                    data.add( new IntegerExpression(-13) );
                    break;
				case x78: // "78":
                    check();
                    data.add( new IntegerExpression(-12) );
                    break;			
				case x79: // "79":
                    check();
                    data.add( new IntegerExpression(-11) );
                    break;
				case x7A: // "7a":
                    check();
                    data.add( new IntegerExpression(-10) );
                    break;
				case x7B: // "7b":
                    check();
                    data.add( new IntegerExpression(-9) );
                    break;
				case x7C: // "7c":
                    check();
                    data.add( new IntegerExpression(-8) );
                    break;
				case x7D: // "7d":
                    check();
                    data.add( new IntegerExpression(-7) );
                    break;
				case x7E: // "7e":
                    check();
                    data.add( new IntegerExpression(-6) );
                    break;
				case x7F: // "7f":
                    check();
                    data.add( new IntegerExpression(-5) );
                    break;
				case x80: // "80":
                    check();
                    data.add( new IntegerExpression(-4) );
                    break;
				case x81: // "81":
                    check();
                    data.add( new IntegerExpression(-3) );
                    break;
				case x82: // "82":
				    check();
                    data.add( new IntegerExpression(-2) );
                    break;
				case x83: // "83":
                    check();
                    data.add( new IntegerExpression(-1) );
                    break;
				case x84: // "84":
					check();
					data.add( new IntegerExpression(0) );
					break;
				case x85: // "85":
					check();
					data.add( new IntegerExpression(1) );
					break;
				case x86: // "86":
                    check();
                    data.add( new IntegerExpression(2) );
                    break;
				case x87: // "87":
                    check();
                    data.add( new IntegerExpression(3) );
                    break;
				case x88: // "88":
                    check();
                    data.add( new IntegerExpression(4) );
                    break;
				case x89: // "89":
                    check();
                    data.add( new IntegerExpression(5) );
                    break;
				case x8A: // "8a":
                    check();
                    data.add( new IntegerExpression(6) );
                    break;
				case x8B: // "8b":
                    check();
                    data.add( new IntegerExpression(7) );
                    break;
				case x8C: // "8c":
                    check();
                    data.add( new IntegerExpression(8) );
                    break;
				case xA9: // "a9":
					check();
					data.add( new ThisExpression() );
					break;
				case xB1: // "b1": // dotget
					check();
					RH = data.remove();
					LH = data.remove();
					expr = new DotExpression(LH, RH);
					data.add( expr );
					break;
				case x1E: // "1e": // not
				    check();
                    expr = data.remove();
                    expr = new NotExpression(expr);
                    data.add( expr );
                    break;
				case x30: // "30": // gt
				    check();
				    RH = data.remove();
				    LH = data.remove();
				    expr = new GTExpression(LH, RH);
				    data.add( expr );
				    break;
				case x32: // "32": // lt
				    check();
                    RH = data.remove();
                    LH = data.remove();
                    expr = new LTExpression(LH, RH);
                    data.add( expr );
                    break;
				case x34: // "34": // eq
                    check();
                    RH = data.remove();
                    LH = data.remove();
                    expr = new EqualExpression(LH, RH);
                    
                    data.add( expr );
                    break;
				case x35: // "35": // neq
                    check();
                    RH = data.remove();
                    LH = data.remove();
                    expr = new NotEqualExpression(LH, RH);
                    
                    data.add( expr );
                    break;
				case xB3: // "b3": // dotset
					check();
					nextToken(); // expecting e3
				
					RH = data.remove();
					LH = data.remove();
					
					LH = new DotExpression(LH, RH);						
					RH = data.remove();
                    expr = new AssignmentExpression(LH, RH);

                    addExpression( expr );
					break;
				case xEF: // "ef":
					check();
					LH = data.remove();
					RH = data.remove();
					expr = new IndexExpression(LH, RH);
					
					data.add( expr );
					break;
				case xF1: // "f1":
				    check();
				    token = nextToken();
				    num = byteToInt( token );
				    
				    LH = data.remove();
				    expressions = new ArrayList<Expression>();
				    for(int k=0; k<num; k++) {
				        expressions.add( data.remove() );
				    }
				    while( expressions.size()>0 ) {
				        RH = expressions.remove( expressions.size()-1 );
				        LH = new IndexExpression(LH, RH);
				    }
				    data.add( LH );
				    break;
				case xF2: // "f2":
                    check();
                    LH = data.remove();
                    RH = data.remove();
                    LH = new IndexExpression(LH, RH);
                    
                    RH = data.remove();
                    expr = new AssignmentExpression(LH, RH);
                    
                    data.add( expr );
                    break;
				case xED: // "ed":
                    check();
                    token = nextToken();
                    num = byteToInt( token );
                    
                    expressions = new ArrayList<Expression>();
                    for(int k=0; k<num; k++) {
                        expressions.add( data.remove() );
                    }
                    Collections.reverse( expressions );
                    expr = new ListExpression( expressions );

                    data.add( expr );
                    break;
				case x11: // "11": // invokespecial
					check();
					token = nextToken();
					num = byteToInt( token );
					
					RH = data.remove();
					LH = data.remove();
					methodExpr = new DotExpression(LH, RH);
					args = new MethodArgument();
					
					for(int i=0; i<num; i++) {
						args.add( data.remove() );
					}
					
					args.reverse();
					expr = new InvokeMethod(methodExpr, args);
					data.add( expr );
					
					token = peekNextToken();
					if( token.value>0 && token.value==xE3 )
					    nextToken(); // case "11 01 e3"
					
					break;				
				case x09: // "09": // if
					check();
					/*token = nextToken();
					b1 = byteToInt( token );
					token = nextToken();
					b2 = byteToInt( token );
					num = ( b1<<8 ) + b2;*/
					
					token = nextToken();
					num = u2ToInt( token, nextToken() );
					
					if( DEV_DEBUG ) {
					    Expression expr1 = data.peek();
					    System.out.println( expr1.toString() );
					    System.out.println();
					}
					
					if( num<0 ) {
				        jump = tokenIndex + num - 3 - 3; // 3 - size of goto bytecode
				        
				        if( gotoMap.containsKey(jump) ) {
				            expr = data.remove();
				            
				            gotoExpr = gotoMap.get( jump );
				            DataPoint dataGoto = gotoExpr.getData();
				            
				            LH = dataGoto.remove();				            
				            RH = gotoExpr.removeExpressionLast();
				            
				            expr = new ForExpression(LH, expr, RH, gotoExpr);
				            
				            while(true && output.size()>0) {
				                Expression exp = removeExpression();
				                if( (exp instanceof GotoExpression) && gotoExpr.equals((GotoExpression)exp) ) break;
				            }
				            
				            addExpression( (ForExpression)expr );
				            
				        } else {
				            throw new FormatException("if-goto with negative jump without corresponding positive goto");
				        }
				        break;
				    } else {					
					
    					expr = data.remove();
    					Expression ifExpr = new IfExpression(expr);
    					                        
    					reverseStack = new Stack<Expression>();
                        while( !data.isEmpty() ) {
                            reverseStack.push( data.remove() );
                        }
                        while( !reverseStack.empty() ) addExpression( reverseStack.pop() );
    					
    					addExpression( ifExpr ); 
    					//indent++;
                        
                        // (2) process code after if
                        num -= 3; // shift to correct offset
                        subroutine = nextTokens( num );
                    
                        proc = new OScriptDecompiler(this, /*indent,*/ subroutine );
                        expressions = proc.codeToExpression();
                                            
                        sz = expressions.size();
                        for(int k=0; k<sz; k++) {
                            addExpression( expressions.get( k ) );
                        }
                        
                        // (3) Restore three last tokens if they are else/end commands
                        sz = subroutine.size();
                        token = subroutine.get( sz-3 );
                        if( token.value>0 && token.value==x03 ) {
                            removeExpression();
                            restorePrevTokens( 3 );
                            break;
                        }
                        
                        // (4) check for else/end
                        token = peekNextToken();
                        if( token.value>0 && !(token.value==x03) ) {
                            //indent--;
                            addExpression( new EndExpression() ); 
                        }
                        
    					break;
				    }
				case x03: // "03": // endif
					check();
					curIndex = tokenIndex - 1;
					
					// TODO: check mask "03 00 03"
					token = nextToken();
					b1 = byteToInt( token );
					token = nextToken();
					b2 = byteToInt( token );
					num = ( b1<<8 ) + b2;
					                    
					if( num==3 ) { // end						
					    reverseStack = new Stack<Expression>();
	                    while( !data.isEmpty() ) {
	                        reverseStack.push( data.remove() );
	                    }
	                    while( !reverseStack.empty() ) addExpression( reverseStack.pop() );
					    
					    if( this.state==State.InsideDefault ) {
						    this.state = State.Normal;
						    
						    //indent--;
	                        addExpression( new EndExpression() ); 
						}
					    
						//indent--;
						addExpression( new EndExpression() ); 
					} else if ( num>3 ) {					    						
						if( tokenIndex+num >= tokenLength ) {
						    addExpression( new BreakExpression() );
	                        //indent--;
						    break;
						}
					    						
						num -= 3; // shift to correct offset
						subroutine = nextTokens( num );
					
						proc = new OScriptDecompiler(this, /*indent,*/ subroutine );
						expressions = proc.codeToExpression();
						
						gotoExpr = new GotoExpression(curIndex, num, expressions, data);
						gotoMap.put(curIndex, gotoExpr);
						
						//indent--;
						addExpression( gotoExpr );
					} else {
						throw new FormatException("incorrect format for end/else 03 xx xx, where xx xx>=00 03");
					}
					
					break;
				case x0B: // "0b": // switch
				    check();
				    curIndex = getTokenIndex() - 1;
				    
				    expr = data.remove();
				    expr = new SwitchExpression( expr );
				    addExpression( expr );
				    //indent++;
				    
				    token = nextToken();
				    num = byteToInt( token );
				    
				    token = nextToken();
				    b1 = byteToInt( token );
				    token = nextToken();
				    b2 = byteToInt( token );
				    
				    defaultJump = (b1<<8) + b2;
				    caseJump = new TreeMap<Integer, Integer>();
				    
				    for(int k=0; k<num; k++) {
				        token = nextToken();
				        b1 = byteToInt( token );
				        token = nextToken();
				        b2 = byteToInt( token );
				        constID = (b1<<8) + b2;
				        
				        token = nextToken();
				        b1 = byteToInt( token );
				        token = nextToken();
				        b2 = byteToInt( token );
				        jump = (b1<<8) + b2;
				        
				        caseJump.put(constID, jump);
				    }   
				    // caseIdx += num*4; // byte section for case jump description
				    
				    // (2) process cases
				    for( Map.Entry<Integer, Integer> e : caseJump.entrySet() ) {
				        oConst = constPool.getConstant( e.getKey() );
				        start = e.getValue();
				        
				        next = caseJump.higherEntry( e.getKey() );
				        if( next!=null ) {
				            end = next.getValue();
				        } else {
				            end = defaultJump;
				        }				        
				        
                        //indent++;
				        subroutine = peekNextTokens(curIndex + start, curIndex + end);
				        //subroutine = peekNextTokens(start, end);
				        
				        proc = new OScriptDecompiler(this, /*indent,*/ subroutine );
				        expressions = proc.codeToExpression();
				        
				        removeLastJump(subroutine, expressions);
				        
				        expr = new CaseExpression( oConst, expressions );
                        addExpression( expr );				              
				    }
				    
				    // (3) process default case
				    moveTokenIndex( curIndex + defaultJump );
				    token = peekNextToken();
				    if( token.value>0 && token.value==x03 ) {
				        //indent--;
                        addExpression( new EndExpression() );
				    } else {
				        this.state = State.InsideDefault;
				        addExpression( new DefaultExpression() );
                        //indent++;
				    }
				    
				    break;
				case x14: // "14": // invokedynamic
					check();
					token = nextToken();
					num = byteToInt( token );
					
					// read next two bytes
					token = nextToken();
					hexCode = token.hex;
					token = nextToken();
					hexCode = String.format("%s %s", hexCode, token.value);
					
					args = new MethodArgument();						
					for(int i=0; i<num; i++) {
						args.add( data.remove() );
					}
					
					args.reverse();
					expr = new InvokeDynamic(hexCode, args);
					data.add( expr );
					
					token = peekNextToken();
                    if( token.value>0 && token.value==xE3 )
                        nextToken();
					
					break;
				case x23: // "23":
					check();
					RH = data.remove();
					LH = data.remove();
				
					data.add(new AddExpression(LH, RH));
					break;
				case x24: // "24":
                    check();
                    RH = data.remove();
                    LH = data.remove();
                
                    data.add(new SubExpression(LH, RH));
                    break;
				case xE6: // "e6": // or && and
					check();
					
					swToken = nextToken();
					char val = swToken.value;
					if( val==x00 ) break;
					if( !( val==xEB || val==x05 || val==x08 ) ) break;
					
					if( swToken.value>0 && swToken.value==xEB ) {
					    processForExpression(data);
					    break;
					}
					
					token = nextToken();
					num = byteToInt( token );
					token = nextToken(); // expecting e3
					
					num -= 3; // shift to correct offset
					subroutine = nextTokens( num );
					
					proc = new OScriptDecompiler(this, /*indent,*/ subroutine );
					expressions = proc.codeToExpression();
										
					LH = data.remove();
					RH = concatExpressions( expressions );
					
                    switch (swToken.value) {
                        case x05: // "05": // or
                            expr = new OrExpression(LH, RH);
                            break;
                        case x08: // "08": // and
                            expr = new AndExpression(LH, RH);
                            break;
                        default:
                            expr = new BasicExpression();
                            break;
                    }
					
					data.add( expr );
					break;
				case x08: // "08":
				    check();
				    token = nextToken();
				    num = byteToInt( token );
				    num -= 2; // two bytes were already processed
				    if( num<=0 ) break;
				    
				    expr = data.remove();
				    
				    subroutine = nextTokens( num-2 );
				    proc = new OScriptDecompiler(this, /*indent,*/ subroutine );
				    expressions = proc.codeToExpression();
				    
				    LH = concatExpressions( expressions );
				    
				    subroutine = nextTokens( 2 );
				    // TODO: check subroutine.get(0).equals("08")
				    num = byteToInt( subroutine.get(1) );
				    num -= 2;
				    
				    subroutine = nextTokens( num );
				    proc = new OScriptDecompiler(this, /*indent,*/ subroutine );
				    expressions = proc.codeToExpression();
				    
				    RH = concatExpressions( expressions );
				    
				    expr = new ConditionalExpression(expr, LH, RH);
				    data.add( expr );
				    
				    break;
				case xE1: // "e1":
				    check();
				    token = nextToken();
				    num = byteToInt(token);
				    sz = data.size();
				    if( num<sz ) {
				        expr = data.get( sz-1-num );
				        data.add( expr );
				    }
				    break;
				// I don't have a slightest idea for a meaning to e2, e4, e5, e7
				case xE2: // "e2":
				case xE4: // "e4":
				case xE7: // "e7":
				case xB4: // "b4"
                    check();
                    token = nextToken();
                    break;
				case xE5: // "e5":
                    check();
                    break;
				case x18: // "18":
					check();
					expr = data.remove();
                    addExpression( new ReturnExpression(expr) );
					break;
				default:						
					if( !unknownBytecode ) {
						//output.Append( Environment.NewLine );
						unknownBytecode = true;
					}
				
					lineBuf.append( token.value ).append(" ");
				
					break;				
			}
		}
		
		} catch(FormatException ex) {
		    ex.printStackTrace();
		} catch(Exception ex) {
		    System.out.println("General exception");
            ex.printStackTrace();
        }
		
		// TODO:
		//output.Append( String.Format("Dump Stack: {0}", data.DumpStack()) );
		while( !data.isEmpty() ) {
			output.add(outIdx++, data.remove());
		}
		
        return output;
	}	

	
}
