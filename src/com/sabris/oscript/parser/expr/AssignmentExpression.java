package com.sabris.oscript.parser.expr;

import java.lang.String;

public class AssignmentExpression extends BasicExpression {
    public AssignmentExpression(Expression local, Expression expr) {
        text = String.format("%1$s = %2$s", local.toString(), expr.toString());
    }
}
