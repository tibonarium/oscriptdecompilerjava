package com.sabris.oscript.parser.expr;

import com.sabris.oscript.parser.struct.OScriptConstant;

public class BasicExpression extends Expression {
	public BasicExpression() { text = ""; }
	public BasicExpression(String input) { text = input; }
	public BasicExpression(OScriptConstant oConst) { text = oConst.toString(); }
	
	@Override
	public String toString() { return text; }
	/*@Override
	public String toText() { return text; }*/
	
	@Override
    public String dotAccess() { return String.format("( %s )", text); }
	
	@Override
	public int toString(StringBuilder sb, int indent) {
	    sb.append( text );
	    return indent;
	}
}
