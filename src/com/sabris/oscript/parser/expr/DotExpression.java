package com.sabris.oscript.parser.expr;

public class DotExpression extends BasicExpression {
    public DotExpression(Expression expr, Expression str) {
        text = String.format("%1$s.%2$s", expr.toString(), str.dotAccess());
    }
}
