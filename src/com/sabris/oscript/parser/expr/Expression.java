package com.sabris.oscript.parser.expr;

import java.lang.String;
import java.util.Collections;

public abstract class Expression {
	protected String text;
	public int indent = 0;

	public Expression() {
		text = "";
	}
	
	/*public String toString() { return text; }
	//public String toText() { return text; }
	public String dotAccess() { return text; }
	
	public int toString(StringBuilder sb, int indent) { 
	    String padding = "";
	    if( indent>0 )
            padding = String.join("", Collections.nCopies(indent, "\t"));
        sb.append( padding ).append( this.text );
        sb.append( System.lineSeparator() );
        
        return indent;
	}*/
	
	public abstract String toString();
    public abstract String dotAccess();
    
    public abstract int toString(StringBuilder sb, int indent);
	
	public boolean isEmpty() { return text.isEmpty(); }
}
