package com.sabris.oscript.parser.expr;

public class GlobalVarExpression extends BasicExpression {
    public GlobalVarExpression(Expression str) {
        text = String.format("$%s", str.dotAccess());
    }
}
