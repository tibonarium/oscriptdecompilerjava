package com.sabris.oscript.parser.expr;

public class IndexExpression extends BasicExpression {
    public IndexExpression(Expression LH, Expression RH) {
        text = String.format("%1$s[ %2$s ]", LH.toString(), RH.toString());
    }
}
