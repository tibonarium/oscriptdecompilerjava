package com.sabris.oscript.parser.expr;

public class IntegerExpression extends Expression {
    public IntegerExpression() { text = ""; }
    public IntegerExpression(int val) { text = String.format("%d", val); }
    
    @Override
    public String toString() { return text; }
    @Override
    public String dotAccess() { return String.format("( %s )", text); }
    
    @Override
    public int toString(StringBuilder sb, int indent) {
        sb.append( text );
        return indent;
    }
}
