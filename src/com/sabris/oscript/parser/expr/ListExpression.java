package com.sabris.oscript.parser.expr;

import java.util.Iterator;
import java.util.List;

public class ListExpression extends BasicExpression {
    public ListExpression(List<Expression> exprs) {
        String result;
        StringBuilder sb = new StringBuilder();
        Iterator<Expression> itr = exprs.iterator();
        
        sb.append("{");
        while( itr.hasNext() ) {
            sb.append(" ").append( itr.next() ).append(" ,");
        }
        result = sb.toString();
        if( result.length()>1 ) {
            result = result.substring(0, result.length()-1);
        }        
        result += "}";
        
        this.text = result;
    }
}
