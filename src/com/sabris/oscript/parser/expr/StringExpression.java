package com.sabris.oscript.parser.expr;

import com.sabris.oscript.parser.struct.OScriptConstant;

public class StringExpression extends Expression {
    public StringExpression() { text = ""; }
    public StringExpression(String input) { text = input; }
    public StringExpression(OScriptConstant oConst) { text = oConst.toString(); }
    
    @Override
    public String toString() { return String.format("\"%s\"", text); }
    /*@Override
    public String toText() { return text; }*/
    
    @Override
    public String dotAccess() { return text; }
    
    @Override
    public int toString(StringBuilder sb, int indent) {
        sb.append( String.format("\"%s\"", text) );
        return indent;
    }
}
