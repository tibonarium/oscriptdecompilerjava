package com.sabris.oscript.parser.expr;

public class ThisExpression extends BasicExpression {
    private boolean useExplicitForm;
    
    public ThisExpression() {
        useExplicitForm = false;
    }
    
    public ThisExpression(boolean flag) {
        useExplicitForm = flag;
    }
    
    @Override
    public String toString() {
        if( useExplicitForm )
            return "";
        else
            return "this"; 
    }
    public String toText() { 
        if( useExplicitForm )
            return "";
        else
            return "this";
    }
}
