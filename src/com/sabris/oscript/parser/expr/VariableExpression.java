package com.sabris.oscript.parser.expr;

import com.sabris.oscript.parser.struct.OScriptVariable;

public class VariableExpression extends BasicExpression {
    public VariableExpression(OScriptVariable oVar) {
        text = oVar.name;
    }
}
