package com.sabris.oscript.parser.expr.func;

import com.sabris.oscript.parser.expr.BasicExpression;

import java.util.Map;
import java.util.HashMap;

public class InvokeDynamic extends BasicExpression {
    static Map<String, String> hexCore = new HashMap<String, String>();
    static {
        //hexCore.put("", "");
        hexCore.put("02 00", "Type");
        hexCore.put("02 01", "Length");
        hexCore.put("02 02", "Echo");
        hexCore.put("02 03", "IsDefined");
        hexCore.put("02 04", "IsUndefined");
        hexCore.put("02 05", "IsError");
        hexCore.put("02 0f", "IsFeature");
        
        hexCore.put("07 19", "Str.Format");
        
        hexCore.put("f3 19", "Date.Tick");
        
        hexCore.put("8c 01", "RecArray.addRecord");
        
        hexCore.put("1b 0e", "OS.Features");
        hexCore.put("1b 08", "OS.DeleteFeature");
        hexCore.put("1b 0c", "OS.IsObject");
        hexCore.put("1b 0f", "OS.IsFeature");
        
        hexCore.put("35 07", "CAPI.Exec");
        
        hexCore.put("37 23", "WAPI.FreeWork");
        hexCore.put("37 37", "WAPI.AccessWork");
        
        
        hexCore.put("38 0f", "DAPI.GetNodeByID");
        hexCore.put("38 52", "DAPI.GetVersion");
        
        hexCore.put("e8 00", "Assoc.CreateAssoc");
        
        hexCore.put("fb 01", "Error.Get");
    }
    
    public InvokeDynamic(String hexCode, MethodArgument args) {
        if( args.size()>0 )
            text = String.format("%1$s( %2$s )", coreMethod( hexCode ), args.toString());
        else
            text = String.format("%1$s()", coreMethod( hexCode ));
    }
    
    public String coreMethod(String hexCode ) {
        String name;
        
        if( hexCore.containsKey(hexCode) ) {
            name = hexCore.get(hexCode);
        } else {
            name = String.format("<%s>", hexCode);
        }
        return name;
    }
}
