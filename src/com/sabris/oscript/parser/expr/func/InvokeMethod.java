package com.sabris.oscript.parser.expr.func;

import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.BasicExpression;

public class InvokeMethod extends BasicExpression {
    public InvokeMethod(Expression method, MethodArgument args) {
        if( args.size()>0 )
            text = String.format("%1$s( %2$s )", method.toString(), args.toString());
        else
            text = String.format("%1$s()", method.toString());
    }
}
