package com.sabris.oscript.parser.expr.func;

import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.StringExpression;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class MethodArgument extends BasicExpression  {
    public List<Expression> args;
    public final static int MAX_INLINE_LENGTH = 50;
    
    public MethodArgument() {
        args = new ArrayList<Expression>();
    }
    
    public MethodArgument add(String val) {
        args.add( new StringExpression( val ) );
        return this;
    }
    
    public MethodArgument add(Expression input) {
        args.add( input );
        return this;
    }
    
    public MethodArgument reverse() {
        Collections.reverse( args );
        return this;
    }
    
    public int size() { return args.size(); }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String result;
        String padding = "";

        if( args.size()<=MAX_INLINE_LENGTH ) {
            padding = ", ";
            for (Expression arg : args) {
                sb.append( arg.toString() ).append( padding );
            }
        } else {
            padding = ", \\" + System.lineSeparator();
            for (Expression arg : args) {
                sb.append( arg.toString() ).append( padding );
            }
        }
        
        result = sb.toString();
        
        if( result.length()>=padding.length() )
            result = result.substring(0, result.length()-padding.length());
        else
            result = "";
        
        return result;
    }
}
