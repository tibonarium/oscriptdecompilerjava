package com.sabris.oscript.parser.expr.logic;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;

public class ConditionalExpression extends BasicExpression {
    public ConditionalExpression(Expression MH, Expression LH, Expression RH) {
        text = String.format("%1$s ? %2$s : %3$s", MH.toString(), LH.toString(), RH.toString());
    }
}
