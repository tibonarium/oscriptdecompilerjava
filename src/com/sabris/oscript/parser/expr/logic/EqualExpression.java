package com.sabris.oscript.parser.expr.logic;

import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.BasicExpression;

public class EqualExpression extends BasicExpression {
    public EqualExpression(Expression LH, Expression RH) {
        text = String.format("%1$s == %2$s", LH.toString(), RH.toString());
    }
}
