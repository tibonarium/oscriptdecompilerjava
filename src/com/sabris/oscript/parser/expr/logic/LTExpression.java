package com.sabris.oscript.parser.expr.logic;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;

public class LTExpression extends BasicExpression {
    public LTExpression(Expression LH, Expression RH) {
        text = String.format("%1$s < %2$s", LH.toString(), RH.toString());
    }
}
