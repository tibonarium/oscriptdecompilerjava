package com.sabris.oscript.parser.expr.logic;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;

public class NotExpression extends BasicExpression {
    public NotExpression(Expression expr) {
        text = String.format("!%s", expr.toString());
    }
}
