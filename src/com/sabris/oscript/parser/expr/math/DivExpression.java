package com.sabris.oscript.parser.expr.math;

import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.BasicExpression;

public class DivExpression extends BasicExpression {
    public DivExpression(Expression LH, Expression RH) {
        text = String.format("%1$s / %2$s", LH.toString(), RH.toString());
    }
}
