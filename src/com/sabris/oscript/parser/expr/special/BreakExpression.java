package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;

public class BreakExpression extends BasicExpression {
    public BreakExpression() {
        text = "break";
    }
}
