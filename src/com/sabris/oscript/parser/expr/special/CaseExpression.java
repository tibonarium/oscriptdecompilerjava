package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.struct.OScriptConstant;

import java.util.List;

public class CaseExpression extends BasicExpression {
    public List<Expression> expressions;
    
    public CaseExpression(OScriptConstant oConst, List<Expression> _expressions) {
        text = String.format("case %s", oConst.toString());
        expressions = _expressions;
    }
    
    @Override
    public int toString(StringBuilder sb, int indent) { 
        indent = super.toString(sb, indent);
        
        indent++;
        int sz = expressions.size();
        for(int j=0; j<sz; j++) {
            indent = expressions.get(j).toString(sb, indent);
        }
        //indent--;
        indent = (new EndExpression()).toString(sb, indent);
        
        return indent;
    }
}
