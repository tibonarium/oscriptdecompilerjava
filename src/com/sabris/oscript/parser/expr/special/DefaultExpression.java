package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;

public class DefaultExpression extends BasicExpression {
    public DefaultExpression() {
        text = "default";
    }
    
    @Override
    public int toString(StringBuilder sb, int indent) { 
        indent = super.toString(sb, indent);
        indent++;
        return indent;
    }
}
