package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;

public class ElseExpression extends BasicExpression {
    public ElseExpression() {
        text = "else";
    }
}
