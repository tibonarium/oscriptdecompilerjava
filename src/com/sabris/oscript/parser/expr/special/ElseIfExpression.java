package com.sabris.oscript.parser.expr.special;

import java.util.Collections;

import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.BasicExpression;

public class ElseIfExpression extends BasicExpression {
    public Expression condExpr;
    
    public ElseIfExpression(Expression expr) {
        if(expr instanceof IfExpression) {
            text = String.format("elseif( %s )", ((IfExpression)expr).condExpr);
            condExpr = ((IfExpression)expr).condExpr;
        } else {
            text = String.format("if( %s )", expr.toString());
            condExpr = expr;
        }
    }
      
    @Override
    public int toString(StringBuilder sb, int indent) { 
        String padding = "";
        if( indent>0 )
            padding = String.join("", Collections.nCopies(indent, "\t"));
        sb.append( padding ).append( this.text );
        sb.append( System.lineSeparator() );
        indent++;
        return indent;
    }
}
