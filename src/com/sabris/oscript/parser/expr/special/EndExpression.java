package com.sabris.oscript.parser.expr.special;

import java.util.Collections;

import com.sabris.oscript.parser.expr.BasicExpression;

public class EndExpression extends BasicExpression {
    public EndExpression() {
        text = "end";
    }
    
    public int toString(StringBuilder sb, int indent) { 
        String padding = "";
        indent--;
        if( indent>0 )
            padding = String.join("", Collections.nCopies(indent, "\t"));
        sb.append( padding ).append( this.text );
        sb.append( System.lineSeparator() );
        
        return indent;
    }
}
