package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;

public class ForExpression extends BasicExpression {
    private ForHeaderExpression header;
    private GotoExpression subset;
    
    public ForHeaderExpression getHeader() { return header; }
    public GotoExpression getSubset() { return subset; }
    
    public ForExpression(Expression LH, Expression expr, Expression RH, GotoExpression gotoExpr) {
        header = new ForHeaderExpression(LH, expr, RH);
        subset = gotoExpr;
    }
    
    public class ForHeaderExpression extends BasicExpression {
        public ForHeaderExpression(Expression LH, Expression Cond, Expression RH) {
            text = String.format("for(%s ; %s ; %s)", LH.toString(), Cond.toString(), RH.toString());
        }
    }
    
    @Override
    public int toString(StringBuilder sb, int indent) { 
        indent = header.toString(sb, indent);
        indent++;
        indent = subset.subroutineToString(sb, indent);
        return indent;
    }
}

