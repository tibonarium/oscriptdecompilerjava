package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;

public class ForeachExpression extends BasicExpression {
    public ForeachExpression(Expression LH, Expression RH) {
        text = String.format("for %s in %s", LH.toString(), RH.toString());
    }
    
    @Override
    public int toString(StringBuilder sb, int indent) { 
        indent = super.toString(sb, indent);
        indent++;
        return indent;
    }
}
