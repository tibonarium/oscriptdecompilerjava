package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.struct.ByteToken;
import com.sabris.oscript.parser.support.DataPoint;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class GotoExpression extends BasicExpression {
    public List<Expression> expressions;
    public int begin;
    public int jump; // jump size after bytecode 03 xx xx
    public DataPoint data;
    
    public DataPoint getData() { return data; }
    public List<Expression> getExpressions() { return expressions; }
    
    public GotoExpression(Integer curIndex, Integer jump, List<Expression> exprs, DataPoint _data) {
        expressions = exprs;
        this.begin = curIndex;
        this.jump = jump;
        int sz = _data.size();
        data = new DataPoint();
        
        for(int k=0; k<sz; k++) {
            data.add( _data.get(k) );
        }
    }
    
    public Expression removeExpressionLast() {
        int sz = expressions.size();
        return expressions.remove( sz-1 );
    }
    
    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof GotoExpression)) return false;
        
        GotoExpression expr = (GotoExpression)other;
        return this.begin==expr.begin;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        int sz = expressions.size();
        String padding = "";
        
        if( jump>0 ) {
            sb.append( (new ElseExpression()).toString() );
            sb.append( System.lineSeparator() );
        }
        
        int pad = indent>0 ? indent+1 : 0;
        if( pad>0 )
            padding = String.join("", Collections.nCopies(pad, "\t"));
        else
            padding = "";
        
        for(int j=0; j<sz; j++) {
            sb.append( padding );
            sb.append( expressions.get(j).toString() );
            sb.append( System.lineSeparator() );
        }
        return sb.toString();
    }
    
    @Override
    public int toString(StringBuilder sb, int indent) { 
        int sz = expressions.size();
        int j=0;
        if( jump>0 ) {
            if( expressions.size()>0 && (expressions.get(0) instanceof IfExpression) ) {
                indent--;
                indent = (new ElseIfExpression( (IfExpression)expressions.get(0) )).toString(sb, indent);
                indent--;
                j++;
            } else {            
                indent--;
                indent = (new ElseExpression()).toString(sb, indent);
            }
        }
        
        indent++;
        for(; j<sz; j++) {
            indent = expressions.get(j).toString(sb, indent);
        }
        //indent--;
        indent = (new EndExpression()).toString(sb, indent);
        
        return indent;
    }
    
    public int subroutineToString(StringBuilder sb, int indent) {
        int sz = expressions.size();
        //indent++;
        for(int j=0; j<sz; j++) {
            indent = expressions.get(j).toString(sb, indent);
        }
        //indent--;
        indent = (new EndExpression()).toString(sb, indent);
        
        return indent;
    }
}
