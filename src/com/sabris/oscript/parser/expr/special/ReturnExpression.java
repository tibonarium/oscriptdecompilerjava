package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.BasicExpression;

public class ReturnExpression extends BasicExpression {
    public ReturnExpression(Expression expr) {
        text = String.format("return %s", expr.toString());
    }
}
