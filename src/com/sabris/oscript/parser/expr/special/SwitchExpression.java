package com.sabris.oscript.parser.expr.special;

import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.Expression;

public class SwitchExpression extends BasicExpression {
    public SwitchExpression(Expression expr) {
        text = String.format("switch( %s )", expr.toString());
    }
    
    @Override
    public int toString(StringBuilder sb, int indent) { 
        indent = super.toString(sb, indent);
        indent++;
        return indent;
    }
}
