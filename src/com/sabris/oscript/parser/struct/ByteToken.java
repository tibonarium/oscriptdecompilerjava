package com.sabris.oscript.parser.struct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import com.sabris.oscript.parser.FormatException;
import com.sabris.oscript.parser.OScriptDecompiler;
import com.sabris.oscript.parser.OScriptDecompiler.OType;
import com.sabris.oscript.parser.OScriptDecompiler.State;
import com.sabris.oscript.parser.expr.AssignmentExpression;
import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.DotExpression;
import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.GlobalVarExpression;
import com.sabris.oscript.parser.expr.IndexExpression;
import com.sabris.oscript.parser.expr.IntegerExpression;
import com.sabris.oscript.parser.expr.ListExpression;
import com.sabris.oscript.parser.expr.StringExpression;
import com.sabris.oscript.parser.expr.ThisExpression;
import com.sabris.oscript.parser.expr.VariableExpression;
import com.sabris.oscript.parser.expr.func.InvokeDynamic;
import com.sabris.oscript.parser.expr.func.InvokeMethod;
import com.sabris.oscript.parser.expr.func.MethodArgument;
import com.sabris.oscript.parser.expr.logic.AndExpression;
import com.sabris.oscript.parser.expr.logic.ConditionalExpression;
import com.sabris.oscript.parser.expr.logic.EqualExpression;
import com.sabris.oscript.parser.expr.logic.GTExpression;
import com.sabris.oscript.parser.expr.logic.LTExpression;
import com.sabris.oscript.parser.expr.logic.NotEqualExpression;
import com.sabris.oscript.parser.expr.logic.NotExpression;
import com.sabris.oscript.parser.expr.logic.OrExpression;
import com.sabris.oscript.parser.expr.math.AddExpression;
import com.sabris.oscript.parser.expr.math.SubExpression;
import com.sabris.oscript.parser.expr.special.BreakExpression;
import com.sabris.oscript.parser.expr.special.CaseExpression;
import com.sabris.oscript.parser.expr.special.DefaultExpression;
import com.sabris.oscript.parser.expr.special.EndExpression;
import com.sabris.oscript.parser.expr.special.ForExpression;
import com.sabris.oscript.parser.expr.special.GotoExpression;
import com.sabris.oscript.parser.expr.special.IfExpression;
import com.sabris.oscript.parser.expr.special.ReturnExpression;
import com.sabris.oscript.parser.expr.special.SwitchExpression;
import com.sabris.oscript.parser.support.DataPoint;

public class ByteToken {	
    
    public char value;
	public String hex;
	
	public ByteToken() { hex = ""; value = (char)0; }
	public ByteToken( String input ) { 
	    hex = input; 
	    value = (char)Integer.parseInt(hex, 16);
	}
	
	public ByteToken(char c) {
	    value = c;
	    hex = String.format("%02x", (int)value);
	}
	
	public boolean is( String input) {
		return hex.equals(input);
	}
	
	public int toInt32() {
		return Integer.parseInt(hex, 16);
	}
	
	public char toChar() {
        return (char)Integer.parseInt(hex, 16);
    }
	
	@Override
    public String toString(){
	    return hex;
	}
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof ByteToken)) return false;
	    
	    ByteToken tk = (ByteToken)other;
	    return this.value==tk.value;
	}
	
	public static Long convertSectionToLong(ByteToken[] sec) {
        String val = "";
        for(int i=sec.length-1; i>=0; i--) {
            val = val.concat( sec[i].hex );
        }
        return Long.parseUnsignedLong(val, 16);
    }
    
	public static int convertSectionToInteger(ByteToken[] sec) {
        String val = "";
        for(int i=sec.length-1; i>=0; i--) {
            val = val.concat( sec[i].hex );
        }
        return Integer.parseInt(val, 16);
    }
    
    public static OScriptType convertSectionToType(ByteToken[] sec) throws FormatException {
        if( sec.length<2 )
            throw new FormatException("Section can't be less then 2 bytes");
        
        OScriptType type = new OScriptType( OType.UnknownType );
        String val = sec[0].hex.concat( sec[1].hex );
        switch( val ) {
            case "0500": // Boolean
                type = new OScriptType( OType.Boolean );
                break;
            case "0200": // Integer
                type = new OScriptType( OType.Integer );
                break;
            case "ffff": // String
                type = new OScriptType( OType.String );
                break;
            case "80ff": // Object
                type = new OScriptType( OType.Object );
                break;
            case "eeff": // Assoc
                type = new OScriptType( OType.Assoc );
                break;
            case "93ff": // Record
                type = new OScriptType( OType.Record );
                break;
            case "81ff": // Dynamic
                type = new OScriptType( OType.Dynamic );
                break;
            case "efff": // XLate
                type = new OScriptType( OType.XLate );
                break;
            default:
                break;
        }
        return type;
    }
}
