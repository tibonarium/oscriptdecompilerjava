package com.sabris.oscript.parser.struct;

import java.util.List;
import java.util.ArrayList;

public class OScriptCode {
	List<ByteToken> bytes;
	// InstructionSequence seq;
	
	public OScriptCode() {
		bytes = new ArrayList<ByteToken>();
	}
	
	public List<ByteToken> getByteTokens() {
		return bytes;
	}
	
	public void init(String[] input) {
		for(int i=0, sz=input.length; i<sz; i++) {
			bytes.add(new ByteToken( input[ i ] ));
		}
	}
	
	public void init(ByteToken[] input) {
        for(int i=0, sz=input.length; i<sz; i++) {
            bytes.add( input[ i ] );
        }
    }
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int idx = 1;
		
		for(ByteToken token : bytes) {
			sb.append( token.value ).append(" ");
			if( idx % 16 ==0 ) sb.append( System.lineSeparator() );
			idx++;
		}
		return sb.toString();
	}
	
	public String toString(int start, int end) {
		if( start>end ) return "";
		
		StringBuilder sb = new StringBuilder();
		for(int k=start; k<=end; k++) {
			sb.append( bytes.get(k).value );
			sb.append( String.format("[%d]", k) );
			sb.append(" ");
		}
		return sb.toString();
	}
	
}
