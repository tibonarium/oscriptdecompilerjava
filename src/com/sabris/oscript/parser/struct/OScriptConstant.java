package com.sabris.oscript.parser.struct;

import com.sabris.oscript.parser.OScriptDecompiler.OType;

public class OScriptConstant {
	public OType otype;
	public String value;
	public int index;
	public Long offset; // here value for Integer will be stored
	
	public OScriptConstant(OScriptType _type, Long _offset) {
        otype = _type.otype;
		offset = _offset;
		value = "";
		index = 0;
	}
	
	public OScriptConstant(OType _type, String _value) {
        otype = _type;
		value = _value;
		index = 0;
		offset = 0L;
	}
	
	public OScriptConstant(OType _type) {
        otype = _type;
		value = "";
		index = 0;
		offset = 0L;
	}
	
	public String toString() {
	    String result = value;
	    switch(otype) {
	        case Boolean:
	            if(offset==0)
	                result = "FALSE";
	            else if(offset==1)
	                result = "TRUE";
	            else
	                result = String.format("Boolean<hex %d>", offset);
	            break;
	        case XLate:
	            result = String.format("[%s]", value);
	            break;
	        case Integer:
	            result = String.format("%d", offset);
	            break;
	        default:
	            break;
	    }
	    return result;
	}
}
