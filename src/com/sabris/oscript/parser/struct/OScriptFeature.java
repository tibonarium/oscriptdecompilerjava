package com.sabris.oscript.parser.struct;

public class OScriptFeature {
    int szName;
    String name;
    ByteToken[] section;
    ByteToken[] oddSection;
    
    public OScriptFeature(String _name) {
        name = _name;
    }
    
    public OScriptFeature(String _name, int _szName, ByteToken[] _section, ByteToken[] _oddSection) {
        name = _name;
        szName = _szName;
        section = _section;
        oddSection = _oddSection;
    }
}
