package com.sabris.oscript.parser.struct;

import com.sabris.oscript.parser.OScriptDecompiler.OType;

public class OScriptType {
	public OType otype;
	public String description;
	public String hexCode;
	
	// TODO: final?
	String[] descMapping = new String[]{
		"UnknownType"
		,"Boolean"
		,"Date"
		,"Error"
		,"Integer"
		,"List"
		,"Set"
		,"Real"
		,"String"
		,"Assoc"
		,"Object"
		,"RecArray"
		,"Record"
		,"ObjRef"
		,"Dynamic"
		,"Frame"
		,"File"
		,"Script"
		,"Point"
		,"XLate"
	};
	
	String[] hexMapping = new String[]{
		"????" // UnknownType
		,"0500" // Boolean
		,"????" // Date
		,"????" // Error
		,"????" // Integer
		,"????" // List
		,"????" // Set
		,"????" // Real
		,"ffff" // String
		,"eeff" // Assoc
		,"80ff" // Object
		,"????" // RecArray
		,"93ff" // Record
		,"????" // ObjRef
		,"81ff" // Dynamic
		,"????" // Frame
		,"????" // File
		,"????" // Script
		,"????" // Point
		,"efff" // XLate
	};
	
	public OScriptType(OType _type) {
        otype = _type;
		description = descMapping[ _type.toValue() ];
		hexCode = hexMapping[ _type.toValue() ];
	}
	
	public OScriptType(String _hexCode) {
        otype = OType.UnknownType;
		description = "";
		hexCode = _hexCode;
	}
	
	@Override
	public String toString() {
	    return otype.toString();
	}
}
