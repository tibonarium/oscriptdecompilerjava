package com.sabris.oscript.parser.struct;

public class OScriptVariable {
	public OScriptType varType;
	public String name;
	public Long offset;
	public int index;
	
	public String strVal;
	public int intVal;
	
	public OScriptVariable(OScriptType type) {
		varType = type;
		name = "";
		offset = 0L;
	}
}
