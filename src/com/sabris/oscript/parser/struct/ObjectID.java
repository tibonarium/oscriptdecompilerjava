package com.sabris.oscript.parser.struct;

import com.sabris.oscript.parser.FormatException;

public class ObjectID {
    public ByteToken w1;
    public ByteToken w2;
    public ByteToken w3;
    public ByteToken w4;
    
    public ObjectID(ByteToken[] section) throws FormatException {
        if( section.length<4 )
            throw new FormatException("can't create ObjectID section length is less then 4");
            
        w1 = section[0];
        w2 = section[1];
        w3 = section[2];
        w4 = section[3];
    }
    
    @Override
    public String toString() {
        return String.format("%d%d%d%", w4, w3, w2, w1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof ObjectID)) return false;
        
        ObjectID other = (ObjectID) obj;
        return w1==other.w1 && w2==other.w2 && w3==other.w3 && w4==other.w4;
    }
}
