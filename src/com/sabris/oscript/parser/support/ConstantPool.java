package com.sabris.oscript.parser.support;

import com.sabris.oscript.parser.struct.OScriptConstant;
import com.sabris.oscript.parser.OScriptDecompiler.OType;
import com.sabris.oscript.parser.expr.StringExpression;

import java.util.Collection;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;

public class ConstantPool {
	public Map<Integer, OScriptConstant> constants;
	
	public ConstantPool() {
		constants = new HashMap<Integer, OScriptConstant>();
	}
	
	public ConstantPool(String[] _constants) {
		constants = new HashMap<Integer, OScriptConstant>();
		for(int i=0; i<_constants.length; i++) {
			constants.put(i, new OScriptConstant(OType.String, _constants[i]) );
		}
	}
	
	public StringExpression getConstantString(int index) {
		if( constants.containsKey(index) ) {
            return new StringExpression( constants.get(index).toString() );
		} else
			return new StringExpression();
	}
	
	public OScriptConstant getConstant(int index) {
		if( constants.containsKey(index) )
			return constants.get( index );
		else
			return new OScriptConstant(OType.UnknownType);
	}
	
	public Collection<OScriptConstant> getConstants() {
		return constants.values();
	}
	
	public Set<Integer> getKeys() {
		return constants.keySet();
	}
	
	public void add(int index, OScriptConstant oConst) {
		if( !constants.containsKey( index ) )
			constants.put(index, oConst);
	}
}
