package com.sabris.oscript.parser.support;

import com.sabris.oscript.parser.expr.Expression;
import com.sabris.oscript.parser.expr.BasicExpression;
import com.sabris.oscript.parser.expr.StringExpression;

import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class DataPoint {
	public List<Expression> stack;
	
	public DataPoint() {
		stack = new ArrayList<Expression>();
	}
	
	public boolean isEmpty() { return stack.size()==0; }
	
	public int size() { return stack.size(); }
	public Expression get(int i) { return stack.get(i); }
	
	public Expression remove() {
	    int sz = stack.size();
		if( sz>0 )
			return stack.remove( sz-1 );
		else
			return new BasicExpression();
	}
	
	public Expression peek() {
        int sz = stack.size();
        if( sz>0 )
            return stack.get( sz-1 );
        else
            return new BasicExpression();
    }
	
	public StringExpression removeString() {
	    int sz = stack.size();
		if( sz>0 )
			return (StringExpression)stack.remove( sz-1 );
		else
			return new StringExpression();
	}
	public void add(Expression expr) {
		stack.add(expr);
	}
	public String dumpStack() {
		StringBuilder sb = new StringBuilder();
		/*while( !stack.empty() ) {
			sb.append( stack.pop().toString() ).append( System.lineSeparator() );
		}*/
		int sz = stack.size();
		for(int i=sz-1; i>=0; i--) {
		    sb.append( stack.get(i) ).append( System.lineSeparator() );
		}
		return sb.toString();
	}
}
