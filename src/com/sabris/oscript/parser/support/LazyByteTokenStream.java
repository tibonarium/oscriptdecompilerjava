package com.sabris.oscript.parser.support;

import com.sabris.oscript.parser.struct.ByteToken;

public class LazyByteTokenStream extends LazyStream {
    String[] tokenStream;
    int streamLength;
    
    public LazyByteTokenStream(String[] input) {
        tokenStream = input;
        streamLength = input.length;
        
        offset = 0;
        num16ByteRow = 0;
        bufIndex = bufSize; // so that buffer would not be empty
        fillBuffer();
    }
    
    public boolean bufferIsEmpty() {
        return bufIndex >= bufSize;
    }
    
    public boolean endOfStream() {
        return offset>=streamLength;
    }
    
    public void fillBuffer() {      
        if( bufferIsEmpty() && !endOfStream() ) {
            if( offset+bufSize<streamLength ) {
                System.arraycopy(tokenStream, offset, buffer, 0, bufSize);
                offset += bufSize;
            } else {
                int sz = streamLength-offset;
                System.arraycopy(tokenStream, offset, buffer, 0, sz);
                offset += sz;
            }
            bufIndex = 0;
            
            if( offset+16<streamLength) {
                offset += 16; // skip one 16-byte row
                num16ByteRow += 16;
            } else {
                offset = streamLength;
            }
        }
    }
}
