package com.sabris.oscript.parser.support;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import com.sabris.oscript.parser.struct.ByteToken;

public class LazyFileStream extends LazyStream {
    FileInputStream fStream;
    InputStreamReader sReader;
    
    public LazyFileStream(String sourceDir, String sourceName) throws IOException {
        fStream = new FileInputStream(sourceDir + "\\" + sourceName);           
        sReader = new InputStreamReader(fStream, "windows-1251");
        
        offset = 0;
        num16ByteRow = 0;
        bufIndex = bufSize; // so that buffer would not be empty
        fillBuffer();
    }
    
    public boolean bufferIsEmpty() {
        return bufIndex >= bufSize;
    }
    
    public boolean endOfStream() throws IOException {
        return fStream.available()==0;
    }
    
    public void fillBuffer() throws IOException {
        int num;
        if( bufferIsEmpty() && !endOfStream() ) {
            num = sReader.read(buffer, offset, bufSize);
            offset += num;
            
            bufIndex = 0;
            sReader.skip( 16 );
            num16ByteRow += 16;
        }
    }    
}
