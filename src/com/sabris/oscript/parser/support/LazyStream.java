package com.sabris.oscript.parser.support;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sabris.oscript.parser.struct.ByteToken;

public abstract class LazyStream {
    final static int bufSize = 15*16;
    final static char NULL = (char)0x00;
    
    // TODO: char => byte
    char[] buffer = new char[ bufSize ];
    int bufIndex;
    int num16ByteRow;
    int offset;
    
    public abstract boolean bufferIsEmpty();
    
    public abstract boolean endOfStream() throws IOException ;
    
    public abstract void fillBuffer() throws IOException ;
    
    /*public ByteToken[] readBytes(int num) throws IOException;
    
    //public String[] readBytes(Long lNum);
    
    public ByteToken[] readU2() throws IOException;
    
    public ByteToken[] readU4() throws IOException;
    
    public ByteToken[] readU8() throws IOException;
            
    public ByteToken[] readByteTokens(int num) throws IOException;
    
    //public ByteToken[] readByteTokens(Long lNum);
    
    public String readString() throws IOException;
    
    public String readStringWithOffset( int offset ) throws IOException;
    
    public void skipNULL() throws IOException; */
    
    public ByteToken[] readBytes(int num) throws IOException  {
        ByteToken[] a = new ByteToken[num];
            
        for(int k=0; k<num; k++) {
            if(endOfStream() && bufferIsEmpty()) { 
                for(int i=k; i<num; i++) a[i] = new ByteToken(NULL);
                break;
            }
            if( bufferIsEmpty() ) fillBuffer();
            
            a[k] = new ByteToken( buffer[ bufIndex++ ] );
        }
        return a;
    }
    
    /*public ByteToken[] readBytes(Long lNum) {
        List<String> a = new ArrayList<String>();
            
        for(long k=0; k<lNum; k++) {
            if(endOfStream() && bufferIsEmpty()) break;
            if( bufferIsEmpty() ) fillBuffer();
            
            a.add( buffer[ bufIndex++ ] );
        }
        return a.toArray(new String[a.size()]);
    }*/
    
    public ByteToken[] readU2() throws IOException  {
        return readBytes( 2 );
    }
    
    public ByteToken[] readU4() throws IOException  {
        return readBytes( 4 );
    }
    
    public ByteToken[] readU8() throws IOException  {
        return readBytes( 8 );    
    }
            
    public ByteToken[] readByteTokens(int num) throws IOException {
        List<ByteToken> a = new ArrayList<ByteToken>();
            
        for(int k=0; k<num; k++) {
            if( endOfStream() && bufferIsEmpty() ) break;
            if( bufferIsEmpty() ) fillBuffer();                
            
            a.add( new ByteToken( buffer[ bufIndex++ ] ) );             
        }
        return a.toArray(new ByteToken[a.size()]);
    }
    
    public ByteToken[] readByteTokens(Long num) throws IOException {
        List<ByteToken> a = new ArrayList<ByteToken>();
            
        for(long n=0; n<num; n++) {
            if( endOfStream() && bufferIsEmpty() ) break;
            if( bufferIsEmpty() ) fillBuffer();                
            
            a.add( new ByteToken( buffer[ bufIndex++ ] ) );             
        }
        return a.toArray(new ByteToken[a.size()]);
    }
    
    /*public ByteToken[] readByteTokens(Long lNum) throws IOException  {
        List<ByteToken> a = new ArrayList<ByteToken>();
            
        for(long k=0; k<lNum; k++) {
            if( endOfStream() && bufferIsEmpty() ) break;
            if( bufferIsEmpty() ) fillBuffer();                
            
            a.add( new ByteToken( buffer[ bufIndex++ ] ) );             
        }
        return a.toArray(new ByteToken[a.size()]);
    }*/
    
    public String readString() throws IOException {
        StringBuilder sb = new StringBuilder();
        char sym;
        
        while( !endOfStream() ) {
            if( bufferIsEmpty() ) fillBuffer();
            
            sym = buffer[ bufIndex++ ];
            if( sym==NULL ) break;
            
            sb.append( sym );
        }
        return sb.toString();
    }
    
    public String readString(int num) throws IOException {
        StringBuilder sb = new StringBuilder();
        char sym;
        int ln=0;
        
        while( !endOfStream() ) {
            ln++;
            if( bufferIsEmpty() ) fillBuffer();
            
            sym = buffer[ bufIndex++ ];
            if( sym==NULL ) continue;
            
            sb.append( sym );            
            if( ln>=num ) break;
        }
        return sb.toString();
    }
    
    public String readStringWithOffset( int offset ) throws IOException {
        // save current state
        int curOffset = this.offset;
        int curBufIndex = this.bufIndex;
        
        char[] curBuffer = new char[ bufSize ];
        for(int k=0; k<bufSize; k++) {
            curBuffer[k] = this.buffer[k];
        }
        
        // move offset
        this.offset += offset;
        this.bufIndex += offset;
        
        String result = readString();
        
        // restore state
        this.offset = curOffset;
        this.bufIndex = curBufIndex;
        this.buffer = curBuffer;
        
        return result;
    }
    
    public void skipNULL() throws IOException {
        if( bufferIsEmpty() ) fillBuffer();
        char c = buffer[ bufIndex ];
        if( !(c==NULL) ) return;
        
        bufIndex++;
        while( !endOfStream() ) {
            if( bufferIsEmpty() ) fillBuffer();
            c = buffer[ bufIndex++ ];
            if( !(c==NULL) ) break;
        }
        bufIndex--;
    }
}
