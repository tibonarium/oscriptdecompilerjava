package com.sabris.oscript.parser.support;

import com.sabris.oscript.parser.struct.OScriptType;
import com.sabris.oscript.parser.struct.OScriptVariable;
import com.sabris.oscript.parser.OScriptDecompiler.OType;
import com.sabris.oscript.parser.FormatException;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.*;

public class VariablePool {
	public Map<Integer, OScriptVariable> arguments;
	public Map<Integer, OScriptVariable> locals;
	
	public int numArgs;
	public int numLocal;
	
	public VariablePool() {
		arguments = new HashMap<Integer, OScriptVariable>();
		locals = new HashMap<Integer, OScriptVariable>();
	}

	public Stream<OScriptVariable> getVariables() {
		return Stream.concat( locals.values().stream(), arguments.values().stream());
	}
	
	public void setOffsetWithKey(int index, Long offset) {
		if(index<numArgs) {
	        if( arguments.containsKey( index ) ) {
	            arguments.get( index ).offset = offset;
	        }
	    } else {
	        index -= numArgs;
	        if( locals.containsKey( index ) ) {
                locals.get( index ).offset = offset;
            }
	    }
	}
	
	public void setArgumentOffsetWithKey(int index, Long offset) {
        if( arguments.containsKey( index ) ) {
            arguments.get( index ).offset = offset;
        }
    }
	
	public void setLocalOffsetWithKey(int index, Long offset) {
        if( locals.containsKey( index ) ) {
            locals.get( index ).offset = offset;
        }
    }
	
	public void add(int index, OScriptType type) {
		OScriptVariable oVar;
		if( !locals.containsKey( index ) ) {
			oVar = new OScriptVariable(type);
			oVar.index = index;
			locals.put(index, oVar);
		}
	}
	
	public void addArgument(int index, OScriptType type) {
		OScriptVariable oVar;
		if( !arguments.containsKey( index ) ) {
			oVar = new OScriptVariable(type);
			oVar.index = index;
			arguments.put(index, oVar);
		}
	}			
	
	public void addLocal(int index, OScriptType type) {
		OScriptVariable oVar;				
		if( !locals.containsKey( index ) ) {
			oVar = new OScriptVariable(type);
			oVar.index = index;
			locals.put(index, oVar);
		}
	}
	
	public Set<Integer> getKeys() {
		return locals.keySet();
	}
	
	public OScriptVariable getVariable(int index) {
		OScriptVariable oVal;
		if(index<numArgs) {
		
			if( arguments.containsKey( index ) )
				oVal = arguments.get( index );
			else
				oVal = new OScriptVariable(new OScriptType( OType.UnknownType ));
		} else {
			index = index - numArgs;
			if( locals.containsKey( index ) )
				oVal = locals.get( index );
			else
				oVal = new OScriptVariable(new OScriptType( OType.UnknownType ));
		}
		return oVal;
	}
	
	public OScriptVariable getLocal(int index) throws FormatException {
		OScriptVariable oVal;			
		if( locals.containsKey( index ) )
			oVal = locals.get( index );
		else
			throw new FormatException("index is out of locals pool size");
		return oVal;
	}
	
	public OScriptVariable getArgument(int index) throws FormatException {
		OScriptVariable oVal;			
		if( arguments.containsKey( index ) )
			oVal = arguments.get( index );
		else
			throw new FormatException("index is out of argument pool size");
		return oVal;
	}
}
